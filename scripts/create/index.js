import fs from 'fs';
import Categories from './../../src/ExampleCategories.js';
import {
  init,
  finish,
  getOneOf,
  checkFile,
  getAnswer,
  findLine,
  getAnswerTrueFalse,
} from './helpers.js';

console.log('Create new', '\x1b[36m', 'THREE.Project()', '\x1b[0m');

const emptyCubeProject = `import Example from './../example';[SPRITE1]

class [NAME] extends Example {
  constructor() {
    super();[CUBE1][SPRITE2]
  }

  animate(delta) {
    [CUBE2][SPRITE3]
  }
}

export default [NAME];
`;
const codeModules = {
  cube: [
    `

  const geometry = new THREE.BoxGeometry(1, 1, 1);
  const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
  const cube = new THREE.Mesh(geometry, material);
  this.objects.push(cube);
  this.scene.add(cube);
`,
    `
    if (this.objects.length) {
      this.objects[0].rotation.x += 0.01;
      this.objects[0].rotation.y += 0.01;
    }
`,
  ],
  sprite: [
    `
import GameControls from './../../utils/GameControls';
import { Sprite } from 'three-modules';`,
    `

    this.camera.position.set(0, 0, 2);

    const canvas = document.createElement('canvas');
    const texture = new THREE.CanvasTexture(canvas);
    this.texture = texture;
    this.sprite = new Sprite(canvas, texture, 10);
    const geometry = new THREE.PlaneGeometry(1, 1);
    const material = new THREE.MeshBasicMaterial({
      map: texture,
      transparent: true,
      opacity: 1.0,
    });
    this.$ = new THREE.Mesh(geometry, material);
    this.scene.add(this.$);

    fetch('[SPRITEPATH]')
      .then((response) => response.json())
      .then((spriteData) => {
        this.spriteData = spriteData;
        this.sprite.setAssetPath(spriteData.assetPath);
        this.sprite.setAnimations(spriteData.objects[0]);
        this.sprite.playContinuous(
          spriteData.objects[0].animations[0].name,
        );
      });

    this.controls.dispose();
    this.controls = new GameControls(
      this.camera,
      this.renderer.domElement,
    );

    this.controls.on('keydown', (e) => {
      if (this.spriteData) {
        this.spriteData.objects[0].animations.forEach((animation, i) => {
          if (+e.key === i + 1) {
            this.sprite.playOnce(animation.name);
          }
        });
      }
    });
`,
    `
    this.sprite.animate(delta);
`,
  ],
};
const examplePath = 'src/examples/';

init();

const getName = () => {
  return getOneOf("What's project name?\n", (input) => {
    const exist = checkFile(examplePath + input);
    let error = null;
    let value = null;

    if (input.indexOf('-') > -1) {
      error = "Project name cannot include '-'";
    } else if (exist) {
      error = 'Project with this name already exists';
    } else {
      value =
        input[0].toUpperCase() +
        input.slice(1, input.length).toLowerCase();
    }

    return [error, input];
  });
};

const getCategory = () => {
  return getOneOf(
    `Choose category: ${Object.values(Categories).join(', ')}\n`,
    (input) => {
      let error = null;
      let value = null;

      const categoryIndex = Object.values(Categories).findIndex(
        (category) =>
          category.toLowerCase().indexOf(input.toLocaleLowerCase()) >
          -1,
      );

      if (categoryIndex === -1) {
        error = 'Invalid category';
      } else {
        value = Object.keys(Categories)[categoryIndex];
      }

      return [error, value];
    },
  );
};

const getInfoPromise = async () => {
  const name = await getName();
  const description = await getAnswer(
    "What's project description?\n",
  );
  const category = await getCategory();
  const cube = await getAnswerTrueFalse(
    'Add Cube to the project (y/n)?\n',
  );
  const object = await getAnswerTrueFalse(
    'Add .obj model to the project (y/n)?\n',
  );
  let objPath;
  if (object) {
    objPath = await getAnswer("What's obj path?\n");
  }
  const sprite = await getAnswerTrueFalse(
    'Add sprite object with gameControls? Sprite animations would be connected to number keys\n',
  );
  let spritePath;
  if (sprite) {
    spritePath = await getAnswer(
      "What's json sprite objects path?\n",
    );
  }

  const modules = { cube, object, objPath, sprite, spritePath };

  return [name, description, category, modules];
};

getInfoPromise().then(([name, description, category, modules]) => {
  const mainIndexPromise = new Promise((resolve, reject) => {
    resolve();

    fs.readFile('src/index.js', 'utf8', function (err, data) {
      if (err) console.log(err);

      const lines = data.split('\n');

      const lineNumber = findLine(lines, '];');

      if (lineNumber > -1) {
        const newLine = `  {
    Demo: Examples.${name},
    name: '${name}',
    description: '${description}',
    category: ExampleCategories.${category},
  },`;
        const newLines = [
          ...lines.slice(0, lineNumber),
          newLine,
          ...lines.slice(lineNumber, lines.length),
        ];

        fs.writeFile(
          'src/index.js',
          newLines.join('\n'),
          'utf8',
          function (err) {
            if (err) console.log(err);

            resolve();
          },
        );
      } else {
        reject('Could not find correct line in examples array.');
      }
    });
  });

  const examplesIndexPromise = new Promise((resolve, reject) => {
    fs.readFile(
      'src/examples/index.js',
      'utf8',
      function (err, data) {
        if (err) console.log(err);

        const lines = data.split('\n');
        const lineNumber = findLine(lines, 'export {');
        let newLines = [];

        if (lineNumber > -1) {
          const newLine = `import ${name} from './${name.toLowerCase()}/${name.toLowerCase()}.js';`;
          newLines = [
            ...lines.slice(0, lineNumber - 1),
            newLine,
            ...lines.slice(lineNumber - 1, lines.length),
          ];
        } else {
          reject('Could not find correct line in imports.');
        }

        const exportLineNumber = findLine(newLines, '};');

        if (exportLineNumber > -1) {
          const newLine = `  ${name},`;
          newLines = [
            ...newLines.slice(0, exportLineNumber),
            newLine,
            ...newLines.slice(exportLineNumber, newLines.length),
          ];
        } else {
          reject('Could not find correct line in exports.');
        }

        fs.writeFile(
          'src/examples/index.js',
          newLines.join('\n'),
          'utf8',
          function (err) {
            if (err) console.log(err);

            resolve();
          },
        );
      },
    );
  });

  const newExamplePromise = new Promise((resolve, reject) => {
    const dirPath = `${examplePath}${name.toLowerCase()}`;
    const filePath = `${dirPath}/${name.toLowerCase()}.js`;

    fs.mkdir(dirPath, { recursive: true }, (err) => {
      if (err) reject(err);

      let projectCode = emptyCubeProject.replace(/\[NAME\]/g, name);

      // for every module, find its [MODULE] and replace with its code or ''
      for (const moduleName in codeModules) {
        const module = codeModules[moduleName];

        module.forEach((codeSample, i) => {
          const codeToReplace = `[${moduleName.toUpperCase()}${
            i + 1
          }]`;
          let replacement = '';

          if (modules[moduleName]) {
            replacement = codeSample;
          }

          projectCode = projectCode.replace(
            codeToReplace,
            replacement,
          );
        });
      }

      if (modules.sprite && modules.spritePath) {
        projectCode = projectCode.replace(
          '[SPRITEPATH]',
          modules.spritePath,
        );
      }

      fs.writeFile(filePath, projectCode, (err) => {
        if (err) reject(err);

        resolve();
      });
    });
  });

  Promise.all([
    mainIndexPromise,
    examplesIndexPromise,
    newExamplePromise,
  ]).then(
    () => {
      console.log(
        'Project',
        '\x1b[36m',
        name,
        '\x1b[0m',
        'created! Enjoy!',
      );
      finish();
    },
    (error) => {
      console.log("Didn't save due to errors. Sori boss.");
      console.error(error.message);
    },
  );
});
