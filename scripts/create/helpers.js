import fs from 'fs';
import readline from 'readline';

let rl;

const init = () => {
  rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.on('close', function () {
    process.exit(0);
  });
};

const finish = () => {
  rl && rl.close();
};

const checkFile = (name) => {
  try {
    if (fs.existsSync(name)) {
      return true;
    } else {
      return false;
    }
  } catch (err) {}
};

const getOneOf = (prompt, checkFunction) => {
  return new Promise((resolve) => {
    rl.question(prompt, (input) => {
      const [error, value] = checkFunction(input);

      if (error) {
        console.log(error);
        resolve(getOneOf(prompt, checkFunction));
      } else {
        resolve(value);
      }
    });
  });
};

const getAnswer = (prompt) => {
  return new Promise((resolve) => {
    rl.question(prompt, (description) => {
      resolve(description);
    });
  });
};

const findLine = (lines, str) => {
  return lines.findIndex((line) => line.startsWith(str));
};

const getAnswerTrueFalse = async (prompt) => {
  const answers = ['y', 'n'];
  return await getOneOf(prompt, (input) => {
    const answerIndex = answers.indexOf(input.toLowerCase());
    let error = null;
    let value = null;

    if (answerIndex > -1) {
      value = input === 'y';
    } else {
      error = "Write 'y' or 'n'";
    }

    return [error, value];
  });
};

export {
  init,
  finish,
  checkFile,
  getOneOf,
  getAnswer,
  findLine,
  getAnswerTrueFalse,
};
