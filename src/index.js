import * as THREE from 'three';
import * as Examples from './examples/index';
import ExampleCategories from './ExampleCategories';

window.THREE = THREE;

const examples = [
  {
    Demo: Examples.Basic,
    name: 'Basic',
    description:
      'Basic setup: rotated cube, orbit controls, antialias, onResize and remove methods.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.BasicLightning,
    name: 'Basic lightning',
    description:
      'Ambient light / hemispherical light and point/directional/spot lights.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Materials,
    name: 'Materials',
    description: 'Spheres with different materials.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Standardmaterial,
    name: 'Standardmaterial',
    description: 'Standard material editor with presets.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.EnvMaps,
    name: 'EnvMaps',
    description:
      'Cubes and spheres with different env maps and mappings.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Console,
    name: 'Console',
    description: 'Print out something',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Nebula,
    name: 'Nebula',
    description: 'Clouds with some lights and postprocessing.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Page404,
    name: 'Page404',
    description:
      '404 Page: 2 text meshes with RGBShiftShader, BadTVShader, FilmShader.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Audio,
    name: 'Audio',
    description: 'Positional audio.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.ParticlesBasic,
    name: 'Particles basic',
    description:
      'Most basic particles example. Includes buffer geometry.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Particles,
    name: 'Particles',
    description:
      'Like basic example, but with shader material (texture).',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.ParticlesTexture,
    name: 'Particles texture',
    description:
      'Particles with colors from texture, by position on plane not uv.',
    category: ExampleCategories.experiments,
  },
  {
    Demo: Examples.Loop,
    name: 'LoopShader',
    description: '',
    category: ExampleCategories.shaders,
  },
  {
    Demo: Examples.Glow,
    name: 'GlowShader',
    description: 'Glow shader.',
    category: ExampleCategories.shaders,
  },
  {
    Demo: Examples.Mask,
    name: 'Mask',
    description: 'Render masked scenes with simple shader.',
    category: ExampleCategories.shaders,
  },
  {
    Demo: Examples.Texture,
    name: 'TextureShader',
    description: 'Basic fragment shader.',
    category: ExampleCategories.shaders,
  },
  {
    Demo: Examples.LearningShaders,
    name: 'Learning Shaders',
    description: 'Shaders made while i was learning.',
    category: ExampleCategories.shaders,
  },
  {
    Demo: Examples.Tiled,
    name: 'Tiled map',
    description: 'Tiled map. Keyboard controls.',
    category: ExampleCategories.gameMechanichs,
  },
  {
    Demo: Examples.Threejseditor,
    name: 'Threejseditor',
    description:
      'project made with threejs editor and imported as scene',
    category: ExampleCategories.gameMechanichs,
  },
  {
    Demo: Examples.Meshmodel,
    name: 'Mech Model',
    description: 'mech model downloaded from itch.io',
    category: ExampleCategories.gameMechanichs,
  },
  {
    Demo: Examples.Sprite,
    name: 'Sprite',
    description: 'Animated sprite object example',
    category: ExampleCategories.gameMechanichs,
  },
  {
    Demo: Examples.map,
    name: 'Map',
    description: 'Map made of single canvas',
    category: ExampleCategories.gameMechanichs,
  },
  {
    Demo: Examples.map2,
    name: 'Map2',
    description: 'Map made of objects',
    category: ExampleCategories.gameMechanichs,
  },
];
let currentExample = null;
const projectsSelectorList = document.getElementsByClassName(
  'projects-selector--list',
)[0];

let openedMenu =
  (window.localStorage.getItem('openedMenu') || '') === 'true';
const openedCategoriesStorage =
  window.localStorage.getItem('openedCategories') || '';
const openedCategories = openedCategoriesStorage.length
  ? openedCategoriesStorage.split(',')
  : [];
const lastExample = window.localStorage.getItem('lastExample');

const openCategory = (categoryName) => {
  const categoryElement = projectsSelectorList.querySelector(
    `[data-category='${categoryName}']`,
  );
  const categoryNameElement = categoryElement.querySelector('p');

  let namePrefix;
  const categoryIndex = openedCategories.indexOf(categoryName);

  if (categoryIndex > -1) {
    namePrefix = '+ ';
    openedCategories.splice(categoryIndex, 1);
  } else {
    namePrefix = '- ';
    openedCategories.push(categoryName);
  }

  window.localStorage.setItem(
    'openedCategories',
    openedCategories.join(','),
  );
  categoryNameElement.innerHTML = namePrefix + categoryName;
  categoryElement.classList.toggle('opened');
};

for (const categoryId in ExampleCategories) {
  const categoryName = ExampleCategories[categoryId];
  const categoryElement = document.createElement('li');
  const categoryNameElement = document.createElement('p');
  const isOpened = openedCategories.indexOf(categoryName) > -1;

  categoryNameElement.innerHTML =
    (isOpened ? '- ' : '+ ') + categoryName;
  categoryElement.appendChild(categoryNameElement);

  const categoryExamplesElement = document.createElement('ul');
  categoryElement.appendChild(categoryExamplesElement);

  categoryElement.className = 'projects-selector--experiment-item';
  if (isOpened) {
    categoryElement.classList.toggle('opened');
  }
  categoryElement.dataset.category = categoryName;
  categoryElement.addEventListener('click', () =>
    openCategory(categoryName),
  );

  projectsSelectorList.appendChild(categoryElement);
}

const selectExample = (exampleName) => {
  const example = examples.find((e) => e.name === exampleName);

  if (!example) {
    console.error('Wrong example name: ', exampleName);
    return;
  }

  const lastSelectedLi =
    projectsSelectorList.querySelector('li.selected');
  if (lastSelectedLi) {
    lastSelectedLi.className = 'projects-selector--example-item';
  }

  const categoryContainer = projectsSelectorList.querySelector(
    `[data-category='${example.category}'] ul`,
  );
  const selectedLi = categoryContainer.querySelector(
    `[data-example='${example.name}']`,
  );
  selectedLi.className = 'projects-selector--example-item selected';
  if (currentExample) {
    currentExample.remove();
  }
  window.localStorage.setItem('lastExample', example.name);
  currentExample = new example.Demo();
  window.example = currentExample;
};

for (let i = 0; i < examples.length; i++) {
  const exampleElement = document.createElement('li');
  const example = examples[i];
  const titleElement = document.createElement('div');
  const descriptionElement = document.createElement('div');
  const categoryContainer = projectsSelectorList.querySelector(
    `[data-category='${example.category}'] ul`,
  );

  exampleElement.className = 'projects-selector--example-item';

  exampleElement.dataset.example = example.name;
  titleElement.className = 'title';
  titleElement.innerHTML = example.name;
  descriptionElement.className = 'description';
  descriptionElement.innerHTML = example.description;
  exampleElement.appendChild(titleElement);
  exampleElement.appendChild(descriptionElement);
  exampleElement.addEventListener('click', (e) => {
    e.stopPropagation();
    e.preventDefault();
    selectExample(example.name);
    return false;
  });

  categoryContainer.appendChild(exampleElement);
}

const toggleOpenedMenu = (forceState = null) => {
  if (!openedMenu || forceState === true) {
    projectsSelector.classList.add('active');
    openedMenu = true;
    window.localStorage.setItem('openedMenu', 'true');
  } else {
    projectsSelector.classList.remove('active');
    openedMenu = false;
    window.localStorage.removeItem('openedMenu');
  }
};

const projectsSelector = projectsSelectorList.parentNode;
document.addEventListener('keyup', (e) => {
  if (e.key === '`') {
    toggleOpenedMenu();
  }
});
const projectsSelectorMouseOver = () => {
  toggleOpenedMenu(true);
};
const projectsSelectorMouseOut = () => {
  toggleOpenedMenu(false);
};
projectsSelector.addEventListener(
  'mouseover',
  projectsSelectorMouseOver,
);
projectsSelector.addEventListener(
  'mouseout',
  projectsSelectorMouseOut,
);

console.log(`THREE version: ${THREE.REVISION}`);
console.log('window.example = currentExample');

if (openedMenu) {
  projectsSelector.classList.toggle('active');
}
selectExample(lastExample);
