import Example from './example';

class EnvMaps extends Example {
  constructor() {
    super();
    this.loader.load(
      'sphere-space.jpg',
      (texture) => {
        const bgGeometry = new THREE.SphereGeometry(15, 32, 32);
        const bgMaterial = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.BackSide,
        });
        const bg = new THREE.Mesh(bgGeometry, bgMaterial);

        this.objects.push(bg);
        this.scene.add(bg);

        // Cubes

        const cubeGeometry = new THREE.BoxBufferGeometry(1, 1, 1);

        const cube1Material = new THREE.MeshPhongMaterial({
          color: 0xffaaaa,
          flatShading: true,
          envMap: null,
        });

        const cube2Material = cube1Material.clone();
        const cube3Material = cube1Material.clone();

        const envMap1 = this.loader.load('envMap2.png');
        const envMap2 = texture.clone();
        envMap2.needsUpdate = true;
        const envMap3 = texture.clone();
        envMap3.needsUpdate = true;

        envMap1.mapping = THREE.EquirectangularReflectionMapping;
        envMap2.mapping = THREE.EquirectangularReflectionMapping;
        envMap3.mapping = THREE.EquirectangularRefractionMapping;
        cube1Material.envMap = envMap1;
        cube2Material.envMap = envMap2;
        cube3Material.envMap = envMap3;

        const cube1 = new THREE.Mesh(cubeGeometry, cube1Material);
        const cube2 = new THREE.Mesh(cubeGeometry, cube2Material);
        const cube3 = new THREE.Mesh(cubeGeometry, cube3Material);

        cube1.position.x = -2;
        cube3.position.x = 2;
        cube1.position.y = 0.9;
        cube2.position.y = 0.9;
        cube3.position.y = 0.9;

        this.objects.push(cube1);
        this.scene.add(cube1);
        this.objects.push(cube2);
        this.scene.add(cube2);
        this.objects.push(cube3);
        this.scene.add(cube3);

        // Spheres

        const sphereGeometry = new THREE.SphereBufferGeometry(
          1,
          256,
          256,
        );
        const sphere1Material = new THREE.MeshPhongMaterial({
          color: 0xffaaaa,
          flatShading: true,
          envMap: null,
        });
        const sphere2Material = sphere1Material.clone();
        const sphere3Material = sphere1Material.clone();

        const envMap4 = this.loader.load('envMap2.png');
        const envMap5 = texture.clone();
        envMap5.needsUpdate = true;
        const envMap6 = texture.clone();
        envMap6.needsUpdate = true;

        envMap4.mapping = THREE.EquirectangularReflectionMapping;
        envMap5.mapping = THREE.EquirectangularReflectionMapping;
        envMap6.mapping = THREE.EquirectangularRefractionMapping;
        sphere1Material.envMap = envMap4;
        sphere2Material.envMap = envMap5;
        sphere3Material.envMap = envMap6;

        const sphere1 = new THREE.Mesh(
          sphereGeometry,
          sphere1Material,
        );
        const sphere2 = new THREE.Mesh(
          sphereGeometry,
          sphere2Material,
        );
        const sphere3 = new THREE.Mesh(
          sphereGeometry,
          sphere3Material,
        );
        sphere1.position.x = -2;
        sphere1.position.y = -0.9;
        sphere2.position.y = -0.9;
        sphere3.position.x = 2;
        sphere3.position.y = -0.9;

        this.objects.push(sphere1);
        this.scene.add(sphere1);
        this.objects.push(sphere2);
        this.scene.add(sphere2);
        this.objects.push(sphere3);
        this.scene.add(sphere3);
      },
      // onProgress callback currently not supported
      undefined,
      // onError callback
      () => {
        throw new Error('An error happened');
      },
    );

    // lights
    const ambientLight = new THREE.AmbientLight('#404060');
    const directionalLight1 = new THREE.DirectionalLight('#ffbbaa');
    directionalLight1.position.set(-1, 1, 1);
    directionalLight1.target.position.copy(this.scene.position);
    const directionalLight2 = directionalLight1.clone();
    directionalLight2.position.set(1, 1, 1);
    this.scene.add(directionalLight1);
    this.scene.add(directionalLight2);
    this.scene.add(ambientLight);
  }

  animate() {
    // dont animate object 0 - its sphere
    for (let i = 1; i < this.objects.length; i++) {
      this.objects[i].rotation.x += 0.01;
      this.objects[i].rotation.y += 0.01;
    }
  }
}

export default EnvMaps;
