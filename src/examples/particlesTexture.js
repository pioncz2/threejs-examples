import ParticleTextureShader from './../shaders/particleTexture';
import { TIMES, EASING, Animations } from './../utils/animations';
import Example from './example';

const Width = 512;
const Height = 512;
const Distance = -400;
const ParticleCount = Width * Height;
let x = 0;

class Particles2 extends Example {
  constructor() {
    super();

    this.animations = new Animations();
    this.lastRender = 0;

    // create the particle variables
    this.particles = new THREE.BufferGeometry();
    const positions = [];
    const pxPositions = [];
    const sizes = [];
    this.velocities = [];

    const canvas = document.createElement('canvas');
    canvas.width = Width;
    canvas.height = Height;
    this.ctx = canvas.getContext('2d');
    this.texture = new THREE.CanvasTexture(canvas);

    this.imageLoaded = false;
    this.image = new Image(Width, Height);
    this.image.crossOrigin = true;
    this.image.onload = () => {
      this.imageLoaded = true;
      this.drawTexture();
    };
    this.image.src = '/images/background.jpg';

    const pMaterial = new THREE.ShaderMaterial({
      uniforms: {
        time: { value: 1.0 },
        resolution: { value: new THREE.Vector2() },
        particleTexture: { value: this.texture },
      },
      vertexShader: ParticleTextureShader.vertexShader,
      fragmentShader: ParticleTextureShader.fragmentShader,
      blending: THREE.NormalBlending,
      depthTest: false,
      transparent: true,
      vertexColors: true,
      side: THREE.DoubleSide,
    });

    this.particles.sortParticles = true;

    for (let p = 0; p < ParticleCount; p++) {
      const pxX = p % Width;
      const pxY = parseInt(p / Width, 10);

      const pX = pxX - Width / 2;
      const pY = pxY - Height / 2;
      const pZ = Distance;

      positions.push(pX, pY, pZ);
      pxPositions.push(pxX / Width, pxY / Height);

      sizes.push(1.5);

      // create a velocity vector
      this.velocities.push(-Math.random() * 100);
    }

    this.particles.setAttribute(
      'position',
      new THREE.Float32BufferAttribute(positions, 3),
    );
    this.particles.setAttribute(
      'pxPosition',
      new THREE.Float32BufferAttribute(pxPositions, 2),
    );
    this.particles.setAttribute(
      'size',
      new THREE.Float32BufferAttribute(sizes, 1),
    );

    // create the particle system
    this.particleSystem = new THREE.Points(this.particles, pMaterial);

    // add it to the scene
    this.scene.add(this.particleSystem);
    this.objects = [this.particleSystem];

    const basePositions = [...positions];
    this.animations.create({
      easing: EASING.Linear,
      length: 600,
      loop: true,
      update: (progress) => {
        x = progress * 100;
        this.drawTexture();

        let pCount = ParticleCount;
        while (pCount--) {
          const index = pCount * 3 + 2;
          const positionArray =
            this.particles.attributes.position.array;

          positionArray[index] =
            basePositions[index] + this.velocities[pCount] * progress;
        }

        this.particles.attributes.position.needsUpdate = true;
      },
    });
  }

  drawTexture() {
    this.ctx.clearRect(0, 0, Width, Height);

    if (this.imageLoaded) {
      this.ctx.drawImage(this.image, 0, 0, Width, Height);
    }
    this.ctx.beginPath();
    this.ctx.arc(95, 50 + x, 40, 0, 2 * Math.PI);
    this.ctx.stroke();
    this.texture.needsUpdate = true;
  }

  animate() {
    if (this.particleSystem) {
      const delta = Math.min(Date.now() - this.lastRender, 500) / 100;
      this.animations.tick(delta);
      this.lastRender = Date.now();
    }
  }
}

export default Particles2;
