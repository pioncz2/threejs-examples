import Example from './example';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import {
  MaskPass,
  ClearMaskPass,
} from 'three/examples/jsm/postprocessing/MaskPass.js';
import { DotScreenPass } from 'three/examples/jsm/postprocessing/DotScreenPass.js';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader.js';

class Mask extends Example {
  constructor() {
    super();

    this.dotScreenScene = new THREE.Scene();

    this.renderer.autoClear = false;

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

    // POSTPROCESSING
    this.clock = new THREE.Clock();
    const renderTargetParameters = {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      stencilBuffer: true,
    };
    const webglRenderTarget = new THREE.WebGLRenderTarget(
      window.innerWidth,
      window.innerHeight,
      renderTargetParameters,
    );

    this.composer = new EffectComposer(
      this.renderer,
      webglRenderTarget,
    );
    this.composer.renderTarget1.stencilBuffer = true;
    this.composer.renderTarget2.stencilBuffer = true;

    const renderPass = new RenderPass(this.scene, this.camera);
    const maskPass = new MaskPass(this.dotScreenScene, this.camera);
    const screenDotPass = new DotScreenPass();
    const clearMaskPass = new ClearMaskPass();
    const outputPass = new ShaderPass(CopyShader);
    outputPass.renderToScreen = true;

    this.composer.addPass(renderPass);
    this.composer.addPass(maskPass);
    this.composer.addPass(screenDotPass);
    this.composer.addPass(clearMaskPass);
    this.composer.addPass(outputPass);

    const geometryCube = new THREE.BoxGeometry(1, 1, 1);
    const materialCube = new THREE.MeshPhongMaterial({
      color: '#2FA1D6',
      flatShading: true,
      envMap: null,
    });

    const materialPlane = new THREE.MeshPhongMaterial({
      color: '#fff',
      flatShading: true,
      envMap: null,
    });

    const cube1 = new THREE.Mesh(geometryCube, materialCube);
    const cube2 = new THREE.Mesh(geometryCube, materialCube);

    cube1.position.set(0, 0.5, 0);
    cube1.receiveShadow = true;
    cube1.castShadow = true;
    this.scene.add(cube1);
    this.objects.push(cube1);
    cube2.position.set(2, 0.5, 0);
    cube2.receiveShadow = true;
    cube2.castShadow = true;
    this.dotScreenScene.add(cube2);
    this.objects.push(cube2);

    const geometryPlane = new THREE.BoxGeometry(20, 0.01, 20);
    const plane = new THREE.Mesh(geometryPlane, materialPlane);
    plane.receiveShadow = true;
    this.scene.add(plane);
    this.objects.push(plane);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
    this.scene.add(this.ambientLight);
    this.objects.push(this.ambientLight);
    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.2,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    const pointLight = new THREE.PointLight('#ffffdd', 0.7, 500);
    pointLight.position.set(0, 2, 2);
    pointLight.castShadow = true;
    this.scene.add(pointLight);
    this.objects.push(pointLight);

    this.movingLight = pointLight;
    this.movingAngle = 0;

    this.camera.position.y = 5;
    this.camera.position.z = 5;
  }

  animate() {
    if (this.movingLight) {
      this.movingAngle += 1;
      this.movingAngle %= 360;
      this.movingLight.position.x =
        2 * Math.cos((this.movingAngle * Math.PI) / 180);
      this.movingLight.position.z =
        2 * Math.sin((this.movingAngle * Math.PI) / 180);
      this.composer.render(this.clock);
    }
    return false;
  }
}

export default Mask;
