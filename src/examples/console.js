import Example from './example';
import { MathUtils } from 'three';

//TODO:
//metoda do toglowania
//toggle na hotkey?
//wystawienie do window?
//bad tv shader (efekt kamery nocnej?)
//bend?
//code review

class ConsoleExample extends Example {
  constructor() {
    super();

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    const cube = new THREE.Mesh(geometry, material);
    this.objects.push(cube);
    this.scene.add(cube);

    // distance, opacity, curve
    this.console = new Console(this.scene, this.camera);
    setTimeout(() => {
      this.console.log('hi');
    }, 1000);
  }

  animate() {
    for (let i = 0; i < this.objects.length; i++) {
      this.objects[i].rotation.x += 0.01;
      this.objects[i].rotation.y += 0.01;
      const { x, y } = this.objects[i].rotation;
      this.console.log(`Cube rotation: [${x}, ${y}]`);
    }
    this.console.animate();
  }
}

export default ConsoleExample;

// bend this shit
// https://gist.github.com/dImrich/39988ff5078800f4f1c6eec85223cacc
class Console {
  constructor(scene, camera) {
    const width = 800;
    const height = 600;
    this.width = width;
    this.height = height;
    this.canvas = document.createElement('canvas');
    this.canvas.width = width;
    this.canvas.height = height;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.fillStyle = '#FFFFFF';
    this.ctx.fillRect(0, 0, width, height);
    this.texture = new THREE.CanvasTexture(this.canvas);

    const geometry = new THREE.PlaneGeometry(1, 1);
    const material = new THREE.MeshBasicMaterial({
      map: this.texture,
      transparent: true,
      opacity: 0.7,
    });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    const geometry2 = bendPlaneGeometry();
    const material2 = new THREE.LineBasicMaterial({
      color: 0xff0000,
    });
    const cube2 = new THREE.Line(geometry2, material2);
    cube2.position.set(-10, -10, -10);
    scene.add(cube2);

    this.cube = cube;
    this.dist = 0.9;
    this.camera = camera;

    this.messages = [];
  }
  animate() {
    const cubePosition = new THREE.Vector3();
    this.camera.getWorldDirection(cubePosition);
    cubePosition.multiplyScalar(this.dist);
    cubePosition.add(this.camera.position);

    this.cube.position.copy(cubePosition);
    this.cube.setRotationFromQuaternion(this.camera.quaternion);

    const dist = this.dist; // - 0.5;
    const scaleX =
      2 * dist * Math.tan(MathUtils.degToRad(this.camera.fov / 2));
    this.cube.scale.set(scaleX * this.camera.aspect, scaleX, 1);
  }
  log(msg) {
    const messages = this.messages;
    const ctx = this.ctx;
    const width = this.width;
    const height = this.height;
    const fontSize = 20;
    const maxLength = 29;

    messages.unshift(msg);

    // rewrite texture
    this.ctx.fillStyle = '#FFFFFF';
    this.ctx.fillRect(0, 0, width, height);
    for (let i = 0; i < messages.length; i++) {
      ctx.font = `${fontSize}px Calibri`;
      ctx.fillStyle = '#000';
      ctx.fillText(messages[i], 10, height - fontSize * (i + 0.2));
    }
    if (messages.length > maxLength) {
      this.messages = messages.slice(0, maxLength);
    }
    this.texture.needsUpdate = true;
  }
}

//Bend Three JS plane geometry with bezier curve
//Curved plane generation, bezier curve plane,
function bendPlaneGeometry() {
  const curve = new THREE.CubicBezierCurve3(
    new THREE.Vector3(-10, 0, 0),
    new THREE.Vector3(-5, 15, 0),
    new THREE.Vector3(20, 15, 0),
    new THREE.Vector3(10, 0, 0),
  );

  const points = curve.getPoints(50);
  const geometry = new THREE.BufferGeometry().setFromPoints(points);

  return geometry;
}
