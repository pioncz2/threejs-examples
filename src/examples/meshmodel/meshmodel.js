import Example from './../example';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';

class Meshmodel extends Example {
  constructor() {
    super();

    this.camera.position.set(0, 0, 5);

    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.6,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    const directionalLight = new THREE.DirectionalLight(
      '#ffffdd',
      1.2,
    );
    directionalLight.castShadow = true;
    directionalLight.position.set(0, 2, 5);
    this.scene.add(directionalLight);
    this.objects.push(directionalLight);

    const mtlLoaderExample = new MTLLoader();

    mtlLoaderExample.load(
      'models/fieldfighter/FieldFighter.mtl',
      (materials) => {
        materials.preload();

        const loader = new OBJLoader();

        loader.setMaterials(materials);
        loader.load(
          'models/fieldfighter/FieldFighter.obj',
          (object) => {
            object.position.set(0, -3.5, 0);
            directionalLight.target = object;
            this.scene.add(object);
          },
          (xhr) => {
            console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
          },
          (error) => {
            console.log('An error happened', error.message);
          },
        );
      },
    );
  }

  animate() {}
}

export default Meshmodel;
