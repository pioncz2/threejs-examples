import Example from './example';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { FilmPass } from 'three/examples/jsm/postprocessing/FilmPass.js';
import { RGBShiftShader } from 'three/examples/jsm/shaders/RGBShiftShader.js';
import { BadTVShader } from '../lib/BadTVShader.js';
import { FilmShader } from 'three/examples/jsm/shaders/FilmShader.js';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader.js';

class Page404 extends Example {
  constructor() {
    super();

    this.scene.fog.density = 0;
    this.renderer.setClearColor(0x14273c);

    // Shaders setup
    this.clock = new THREE.Clock();
    this.composer = new EffectComposer(this.renderer);
    this.composer.addPass(new RenderPass(this.scene, this.camera));

    const rgbPass = new ShaderPass(RGBShiftShader);
    const badTVPass = new ShaderPass(BadTVShader);
    const filmPass = new ShaderPass(FilmShader);
    const copyPass = new ShaderPass(CopyShader);

    rgbPass.uniforms.angle.value = 0.2 * Math.PI;
    rgbPass.uniforms.amount.value = 0.0017;
    badTVPass.uniforms.distortion.value = 1.9;
    badTVPass.uniforms.distortion2.value = 0.6;
    badTVPass.uniforms.speed.value = 0.1;
    badTVPass.uniforms.rollSpeed.value = 0.046;
    filmPass.uniforms.grayscale.value = 0;
    filmPass.uniforms.sCount.value = 800;
    filmPass.uniforms.sIntensity.value = 0.9;
    filmPass.uniforms.nIntensity.value = 0.4;

    this.badTVPass = badTVPass;

    this.composer.addPass(rgbPass);
    this.composer.addPass(badTVPass);
    this.composer.addPass(filmPass);
    this.composer.addPass(new FilmPass(0.8, 0.325, 256, false));
    this.composer.addPass(copyPass);
    copyPass.renderToScreen = true;

    const fontLoader = new THREE.FontLoader();

    fontLoader.load(
      '/fonts/fff_forward_regular.typeface.json',
      (font) => {
        // text objects
        const text3d1 = new THREE.TextGeometry('4O4', {
          size: 160,
          height: 20,
          curveSegments: 20,
          font,
        });
        text3d1.computeBoundingBox();
        const centerOffset =
          -0.5 *
          (text3d1.boundingBox.max.x - text3d1.boundingBox.min.x);
        const textMaterial = new THREE.MeshBasicMaterial({
          color: 0xff3300,
        });
        const text1 = new THREE.Mesh(text3d1, textMaterial);
        text1.position.x = centerOffset;
        text1.position.y = 100;
        text1.position.z = 0;
        text1.rotation.x = 0;
        text1.rotation.y = Math.PI * 2;

        const text3d2 = new THREE.TextGeometry('NOT FOUND', {
          size: 50,
          height: 25,
          curveSegments: 10,
          font,
        });
        text3d2.computeBoundingBox();
        const centerOffset2 =
          -0.5 *
          (text3d2.boundingBox.max.x - text3d2.boundingBox.min.x);
        const text2 = new THREE.Mesh(text3d2, textMaterial);
        text2.position.x = centerOffset2;
        text2.position.y = 0;
        text2.position.z = 0;
        text2.rotation.x = 0;
        text2.rotation.y = Math.PI * 2;
        this.objects.push(text1);
        this.scene.add(text1);
        this.objects.push(text2);
        this.scene.add(text2);
      },
    );

    this.x = 0;
    this.camera.position.z = 1000;
    this.clock.start();
  }

  animate() {
    if (this.composer) {
      const delta = this.clock.getDelta();
      this.composer.render(delta);
      const parsedX = this.x > 0.5 ? -this.x : this.x;
      this.badTVPass.uniforms.time.value = parsedX;
    }
    this.x += 0.1;
    this.x = this.x > 20 ? 0 : this.x;
    return false;
  }
}

export default Page404;
