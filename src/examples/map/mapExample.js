import Example from './../example';
import Map from './map';
import GameControls from './../../utils/GameControls';

class mapExample extends Example {
  constructor() {
    super();

    this.controls.dispose();
    this.controls = new GameControls(
      this.camera,
      this.renderer.domElement,
    );

    this.camera.position.x = 0;
    this.camera.position.y = 14;
    this.camera.position.z = 0;
    this.camera.lookAt(new THREE.Vector3(0, 0, 0));

    this.scene.add(new THREE.AmbientLight(0xffffff, 0.4));
    this.scene.add(
      new THREE.HemisphereLight('#ffffdd', '#080820', 0.2),
    );

    this.map = new Map();
    const geometryPlane = new THREE.BoxGeometry(20, 0.01, 20);
    const materialPlane = new THREE.MeshPhongMaterial({
      map: this.map.texture,
      flatShading: true,
      envMap: null,
    });
    const plane = new THREE.Mesh(geometryPlane, materialPlane);
    this.scene.add(plane);

    this.map.setPosition({ x: 0, y: 0 });
  }

  animate() {
    this.map.animate();
  }
}

export default mapExample;
