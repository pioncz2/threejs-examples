import MapData from './MapData.json';
import TileData from './TileData.json';

const MapWidth = 512;
const MapHeight = 512;
const MapRows = 24;
const MapColumns = 24;
const MapCellSize = 24;

class map {
  constructor() {
    this.mapData = MapData;

    this.width = MapWidth;
    this.height = MapHeight;
    this.canvas = document.createElement('canvas');
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    this.cellSize = MapCellSize;
    this.ctx = this.canvas.getContext('2d');
    this.texture = new THREE.CanvasTexture(this.canvas);

    this.offset = {
      x: 0,
      y: 0,
    };

    const loader = new THREE.ImageLoader();
    loader.load(
      TileData.assetUrl,
      (image) => {
        this.assetImage = image;
      },
      undefined,
      function () {
        console.error('An error happened.');
      },
    );
  }

  getTile(x, y) {
    const mapTile = this.mapData.find(
      (tile) => tile.x === x && tile.y === y,
    );
    return (
      mapTile && TileData.tiles.find((tile) => tile.id === mapTile.id)
    );
  }

  setPosition({ x, y }) {
    this.offset.x = x;
    this.offset.y = y;
  }

  animate() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.ctx.fillStyle = '#fff';
    this.ctx.fillRect(0, 0, this.width, this.height);

    for (let y = -1; y <= MapRows; y++) {
      for (let x = -1; x <= MapColumns; x++) {
        const tileX =
          Math.floor(this.offset.x) - this.cellSize / 2 + x;
        const tileY =
          Math.floor(this.offset.y) - this.cellSize / 2 + y;

        const tileData = this.getTile(tileX, tileY);

        if (!tileData) {
          continue;
        }

        const tileWidth =
          this.width / this.cellSize + (tileData ? 1 : 0);
        const tileHeight =
          this.height / this.cellSize + (tileData ? 1 : 0);

        const xPx =
          ((x - (this.offset.x % 1)) * this.width) / this.cellSize;
        const yPx =
          ((y - (this.offset.y % 1)) * this.height) / this.cellSize;

        if (tileData.frame && this.assetImage) {
          const frame = tileData.frame;

          this.ctx.drawImage(
            this.assetImage,
            frame.x,
            frame.y,
            frame.w,
            frame.h,
            xPx,
            yPx,
            tileWidth,
            tileHeight,
          );
        }

        if (tileData.color) {
          const color = tileData
            ? tileData.color
            : `rgba(${(x / this.cellSize) * 255},${
                (y / this.cellSize) * 255
              },0.0,${tileData ? '1.0' : '0.2'})`;
          this.ctx.fillStyle = color;
          this.ctx.fillRect(xPx, yPx, tileWidth, tileHeight);
        }
      }
    }

    this.texture.needsUpdate = true;
  }
}

export default map;
