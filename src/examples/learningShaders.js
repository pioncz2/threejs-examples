import {
  Function1,
  Function2,
  Function3,
  Function4,
  Function5,
  Function6,
  Function7,
  Function8,
  Function9,
  Function10,
  Function11,
  Function12,
  Function13,
  Function14,
  Function15,
  Function16,
  Function17,
  Function18,
  Function19,
  Function20,
  Function21,
  Function22,
  Function23,
  Function24,
  Function25,
  Function26,
  Function27,
  Function28,
  Function29,
  Function30,
  Function31,
  Function32,
  Function33,
} from './../shaders/learning/functions';
import OrbitControls from './../utils/OrbitControls';
import Example from './example';

class LearningShaders extends Example {
  constructor() {
    super();

    this.controls.dispose();

    this.controls = new OrbitControls(
      this.camera,
      this.renderer.domElement,
    );
    this.controls.target.z -= 0.01;
    this.controls.target.x = 1;

    this.camera.position.z = 0;
    this.clock = new THREE.Clock();

    this.uniforms = {
      u_time: { type: 'f', value: 1.0 },
      u_resolution: { type: 'v2', value: new THREE.Vector2() },
      u_mouse: { type: 'v2', value: new THREE.Vector2() },
    };

    const createPlane = (shader, position) => {
      const planeGeometry = new THREE.PlaneGeometry(2, 2, 1, 1);
      const material = new THREE.ShaderMaterial({
        uniforms: this.uniforms,
        vertexShader: shader.vertexShader,
        fragmentShader: shader.fragmentShader,
        side: THREE.DoubleSide,
      });
      const mesh = new THREE.Mesh(planeGeometry, material);
      mesh.position.set(position.x, position.y, position.z);
      mesh.lookAt(new THREE.Vector3(0, 0, 0));
      this.scene.add(mesh);
      this.objects.push(mesh);
    };

    createPlane(Function1, { x: -2.5, y: 2.5, z: -5 });
    createPlane(Function2, { x: 0, y: 2.5, z: -5 });
    createPlane(Function3, { x: 2.5, y: 2.5, z: -5 });
    createPlane(Function4, { x: -2.5, y: 0, z: -5 });
    createPlane(Function5, { x: 0, y: 0, z: -5 });
    createPlane(Function6, { x: 2.5, y: 0, z: -5 });
    createPlane(Function7, { x: -2.5, y: -2.5, z: -5 });
    createPlane(Function8, { x: 0, y: -2.5, z: -5 });
    createPlane(Function9, { x: 2.5, y: -2.5, z: -5 });
    createPlane(Function10, { x: 5, y: 2.5, z: -2.5 });
    createPlane(Function11, { x: 5, y: 2.5, z: 0 });
    createPlane(Function12, { x: 5, y: 2.5, z: 2.5 });
    createPlane(Function13, { x: 5, y: 0, z: -2.5 });
    // createPlane(Function14, { x: 5, y: 0, z: 0 });
    createPlane(Function31, { x: 5, y: 0, z: 0 });

    createPlane(Function15, { x: 5, y: 0, z: 2.5 });
    createPlane(Function16, { x: 5, y: -2.5, z: -2.5 });
    createPlane(Function17, { x: 5, y: -2.5, z: 0 });
    createPlane(Function18, { x: 5, y: -2.5, z: 2.5 });
    createPlane(Function19, { x: 2.5, y: 2.5, z: 5 });
    createPlane(Function20, { x: 0, y: 2.5, z: 5 });
    createPlane(Function21, { x: -2.5, y: 2.5, z: 5 });
    createPlane(Function22, { x: 2.5, y: 0, z: 5 });
    createPlane(Function23, { x: 0, y: 0, z: 5 });
    createPlane(Function24, { x: -2.5, y: 0, z: 5 });
    createPlane(Function25, { x: 2.5, y: -2.5, z: 5 });
    createPlane(Function26, { x: 0, y: -2.5, z: 5 });
    createPlane(Function27, { x: -2.5, y: -2.5, z: 5 });

    createPlane(Function28, { x: -5, y: 2.5, z: 2.5 });
    createPlane(Function29, { x: -5, y: 2.5, z: 0 });
    createPlane(Function30, { x: -5, y: 2.5, z: -2.5 });

    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.7,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    window.addEventListener('resize', this.onResize, false);
    window.addEventListener('mousemove', this.onMousemove, false);
    this.onResize();

    this.animating = true;
  }

  onResize = () => {
    this.uniforms.u_resolution.value.x =
      this.renderer.domElement.width;
    this.uniforms.u_resolution.value.y =
      this.renderer.domElement.height;
  };

  onMousemove = (e) => {
    this.uniforms.u_mouse.value.x = e.pageX;
    this.uniforms.u_mouse.value.y = e.pageY;
  };

  animate() {
    if (this.animating) {
      this.uniforms.u_time.value += this.clock.getDelta();
    }
  }

  remove() {
    super.remove();
    this.controls.dispose();
    this.controls = new OrbitControls(this.camera);
  }
}

export default LearningShaders;
