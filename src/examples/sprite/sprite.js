import Example from './../example';
import GameControls from './../../utils/GameControls';
import { Sprite } from 'three-modules';

class SpriteExample extends Example {
  constructor() {
    super();

    this.camera.position.set(0, 0, 2);

    const canvas = document.createElement('canvas');
    const texture = new THREE.CanvasTexture(canvas);
    this.texture = texture;
    this.sprite = new Sprite(canvas, texture, 10);
    const geometry = new THREE.PlaneGeometry(1, 1);
    const material = new THREE.MeshBasicMaterial({
      map: texture,
      transparent: true,
      opacity: 1.0,
    });
    this.$ = new THREE.Mesh(geometry, material);
    this.scene.add(this.$);

    fetch('sprites/nightborne.json')
      .then((response) => response.json())
      .then((spriteData) => {
        this.spriteData = spriteData;
        this.sprite.setAssetPath(spriteData.assetPath);
        this.sprite.setAnimations(spriteData.objects[0]);
        this.sprite.playContinuous(
          spriteData.objects[0].animations[0].name,
        );
      });

    this.controls.dispose();
    this.controls = new GameControls(
      this.camera,
      this.renderer.domElement,
    );

    this.controls.on('keydown', (e) => {
      if (this.spriteData) {
        this.spriteData.objects[0].animations.forEach(
          (animation, i) => {
            if (+e.key === i + 1) {
              this.sprite.playOnce(animation.name);
            }
          },
        );
      }
    });
  }

  animate(delta) {
    this.sprite.animate(delta);
  }
}

export default SpriteExample;
