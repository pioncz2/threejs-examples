import GlowShader from './../shaders/glow';
import Example from './example';

class Glow extends Example {
  constructor() {
    super();

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

    const geometryCube = new THREE.BoxGeometry(1, 1, 1);

    this.clock = new THREE.Clock();
    const materialCube = new THREE.MeshPhongMaterial({
      color: '#2FA1D6',
      flatShading: true,
      envMap: null,
    });

    const materialPlane = new THREE.MeshPhongMaterial({
      color: '#fff',
      flatShading: true,
      envMap: null,
    });

    const cube1 = new THREE.Mesh(geometryCube, materialCube);
    const cube2 = new THREE.Mesh(geometryCube, materialCube);
    const cube3 = new THREE.Mesh(geometryCube, materialCube);

    cube1.position.set(0, 0.5, 0);
    cube1.receiveShadow = true;
    cube1.castShadow = true;
    this.scene.add(cube1);
    this.objects.push(cube1);
    cube2.position.set(-2, 0.5, 0);
    cube2.receiveShadow = true;
    cube2.castShadow = true;
    this.scene.add(cube2);
    this.objects.push(cube2);
    cube3.position.set(2, 0.5, 0);
    cube3.receiveShadow = true;
    cube3.castShadow = true;
    this.scene.add(cube3);
    this.objects.push(cube3);

    const geometryPlane = new THREE.BoxGeometry(20, 0.01, 20);
    const plane = new THREE.Mesh(geometryPlane, materialPlane);
    plane.receiveShadow = true;
    this.scene.add(plane);
    this.objects.push(plane);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
    this.scene.add(this.ambientLight);
    this.objects.push(this.ambientLight);
    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.2,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    this.movingLight = new THREE.PointLight('#ffffdd', 0.7, 500);
    this.movingLight.position.set(0, 2, 2);
    this.movingLight.castShadow = true;
    this.scene.add(this.movingLight);
    this.objects.push(this.movingLight);

    const customMaterial = new THREE.ShaderMaterial({
      uniforms: {
        c: { type: 'f', value: 1.0 },
        p: { type: 'f', value: 1.4 },
        glowColor: { type: 'c', value: new THREE.Color(0xffffff) },
        viewVector: { type: 'v3', value: this.camera.position },
      },
      vertexShader: GlowShader.vertexShader,
      fragmentShader: GlowShader.fragmentShader,
      side: THREE.FrontSide,
      blending: THREE.AdditiveBlending,
      transparent: true,
    });

    this.moonGlow = new THREE.Mesh(geometryCube, customMaterial);
    this.moonGlow.position.set(
      cube1.position.x,
      cube1.position.y,
      cube1.position.z,
    );
    this.moonGlow.scale.multiplyScalar(1.2);
    this.scene.add(this.moonGlow);

    this.movingAngle = 0;
    this.camera.position.y = 5;
    this.camera.position.z = 5;
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  animate() {
    if (this.movingLight) {
      this.movingAngle += 1;
      this.movingAngle %= 360;
      this.movingLight.position.x =
        2 * Math.cos((this.movingAngle * Math.PI) / 180);
      this.movingLight.position.z =
        2 * Math.sin((this.movingAngle * Math.PI) / 180);
    }
  }
}

export default Glow;
