import { EASING } from './../utils/animations';
import Example from './example';

class Audio extends Example {
  constructor() {
    super();

    const geometry = new THREE.BoxGeometry(1, 1, 1);

    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    this.cube = new THREE.Mesh(geometry, material);
    this.cube.position.x = 0;
    this.cube.position.z = -5;
    this.objects.push(this.cube);
    this.scene.add(this.cube);

    this.movingAngle = 270;

    // create an AudioListener and add it to the camera
    const listener = new THREE.AudioListener();
    this.camera.add(listener);

    // load a sound and set it as the PositionalAudio object's buffer
    const audioLoader = new THREE.AudioLoader();

    // create the PositionalAudio object (passing in the listener)
    this.sound = new THREE.PositionalAudio(listener);
    audioLoader.load('/audio/beat1.wav', (buffer) => {
      this.sound.setLoop(true);
      this.sound.setBuffer(buffer);
      this.sound.setRefDistance(20);
    });
    this.objects.push(this.sound);
    this.cube.add(this.sound);

    this.animating = true;
    this.createOverlay();
  }

  createOverlay() {
    this.overlayElement = document.createElement('div');
    this.overlayElement.setAttribute('class', 'overlay');
    this.overlayElement.addEventListener('click', () => {
      this.toggleAudio();
    });
    document.body.appendChild(this.overlayElement);
  }

  toggleAudio() {
    if (!this.sound.isPlaying) {
      this.sound.play();
      this.overlayElement.setAttribute('class', 'overlay hidden');
    } else {
      this.sound.pause();
      this.overlayElement.setAttribute('class', 'overlay');
    }
  }

  removeOverlay() {
    document.body.removeChild(this.overlayElement);
  }

  animate() {
    if (this.animating) {
      this.cube.rotation.x += 0.01;
      this.cube.rotation.y += 0.01;

      if (this.sound.isPlaying) {
        this.movingAngle += 1;
        this.movingAngle %= 360;
        this.cube.position.x =
          5 * Math.cos((this.movingAngle * Math.PI) / 180);
        this.cube.position.z =
          5 * Math.sin((this.movingAngle * Math.PI) / 180);
        const rotationProgress = this.movingAngle + 10;
        const scale =
          1 +
          1 *
            EASING.InOutQuad(
              Math.abs((rotationProgress % 30) / 30 - 0.5),
            );
        this.cube.scale.set(scale, scale, scale);
      }
    }
  }

  remove() {
    super.remove();

    this.removeOverlay();
    if (this.sound.isPlaying) {
      this.sound.stop();
    }
  }
}

export default Audio;
