import MapData from './MapData';
import TileData from './TileData';
import GameControls from './../../utils/GameControls';
import Example from '../example';
import Monsters from './monsters';
import Projectile from './projectile';

const MapWidth = 512;
const MapHeight = 512;
const AttackDelay = 500;

const positionToAngle = (x, y) => {
  let returnAngle = -1;

  if (x === 0 && y < 0) {
    returnAngle = 0;
  } else if (x > 0 && y < 0) {
    returnAngle = 45;
  } else if (x > 0 && y === 0) {
    returnAngle = 90;
  } else if (x > 0 && y > 0) {
    returnAngle = 135;
  } else if (x === 0 && y > 0) {
    returnAngle = 180;
  } else if (x < 0 && y > 0) {
    returnAngle = 225;
  } else if (x < 0 && y === 0) {
    returnAngle = 270;
  } else if (x < 0 && y < 0) {
    returnAngle = 315;
  }
  return returnAngle;
};

class Player {
  constructor(ctx, width, height) {
    this.width = width;
    this.height = height;
    this.ctx = ctx;
    this.x = 0;
    this.y = 0;
    this.lastAttack = 0;
    this.direction = 0;
    this.color = '#ffff00';
    this.rotation = 0;

    this.radius = Math.min(width, height) / 2;
  }

  draw(x, y) {
    const angle = this.rotation;
    this.ctx.translate(x, y);
    this.ctx.rotate((angle * Math.PI) / 180);

    this.ctx.beginPath();
    this.ctx.fillStyle = this.color;
    const r = this.radius;
    this.ctx.moveTo(-r, 1); // left
    this.ctx.lineTo(0, -r); // middle-top
    this.ctx.lineTo(r, 1); // right
    this.ctx.lineTo(-r, 1); // left
    this.ctx.closePath();
    this.ctx.fill();

    this.ctx.beginPath();
    this.ctx.fillStyle = this.color;
    this.ctx.arc(0, 0, this.radius, 0, Math.PI);
    this.ctx.fill();

    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
  }
}

class Map {
  constructor() {
    this.mapData = MapData;
    this.width = MapWidth;
    this.height = MapHeight;
    this.cellSize = 24;

    this.canvas = document.createElement('canvas');
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    this.ctx = this.canvas.getContext('2d');
    this.texture = new THREE.CanvasTexture(this.canvas);
    this.player = new Player(
      this.ctx,
      MapWidth / this.cellSize,
      MapHeight / this.cellSize,
    );
    this.monsters = new Monsters(
      this.ctx,
      MapWidth / this.cellSize,
      MapHeight / this.cellSize,
    );
    this.projectiles = [];

    this.projectiles.push(
      new Projectile({
        ctx: this.ctx,
        x: 1,
        y: 5,
        rotation: 90,
      }),
    );
  }

  getTile(x, y) {
    const mapTile = this.mapData.find(
      (tile) => tile.x === x && tile.y === y,
    );
    return mapTile && TileData.find((tile) => tile.id === mapTile.id);
  }

  playerAttack(now, attack) {
    if (attack && this.player.lastAttack + AttackDelay < now) {
      console.log('attack');
      this.player.lastAttack = now;
    }
  }

  playerMove(newPosition) {
    let newX = 0;
    let newY = 0;
    let newRotation = 0;

    // check if its valid move
    const isMoveValid = (position) => {
      const valid = position.x >= 0 && position.y >= 0;
      const tileData = this.getTile(
        Math.floor(position.x),
        Math.floor(position.y),
      );

      return valid && tileData && tileData.walkable;
    };

    if (
      isMoveValid({
        x: this.player.x + newPosition.x,
        y: this.player.y,
      })
    ) {
      newX = this.player.x + newPosition.x;
      this.player.x = newX;
    }
    if (
      isMoveValid({
        x: this.player.x,
        y: this.player.y + newPosition.y,
      })
    ) {
      newY = this.player.y + newPosition.y;
      this.player.y = newY;
    }

    newRotation = positionToAngle(
      newPosition.x || 0,
      newPosition.y || 0,
    );

    if (newRotation < 0) {
      newRotation = this.player.rotation;
    }

    this.player.rotation = newRotation;
  }

  update(now) {
    this.monsters.update(now);
    this.player.rotation += 0;

    this.ctx.clearRect(0, 0, this.width, this.height);
    this.ctx.fillStyle = '#fff';
    this.ctx.fillRect(0, 0, this.width, this.height);

    for (let y = -1; y <= this.cellSize; y++) {
      for (let x = -1; x <= this.cellSize; x++) {
        const tileX =
          Math.floor(this.player.x) - this.cellSize / 2 + x;
        const tileY =
          Math.floor(this.player.y) - this.cellSize / 2 + y;
        const tileData = this.getTile(tileX, tileY);
        const tileWidth =
          this.width / this.cellSize + (tileData ? 1 : 0);
        const tileHeight =
          this.height / this.cellSize + (tileData ? 1 : 0);

        const color = tileData
          ? tileData.color
          : `rgba(${(x / this.cellSize) * 255},${
              (y / this.cellSize) * 255
            },0.0,${tileData ? '1.0' : '0.2'})`;

        this.ctx.fillStyle = color;
        this.ctx.fillRect(
          ((x - (this.player.x % 1)) * this.width) / this.cellSize,
          ((y - (this.player.y % 1)) * this.height) / this.cellSize,
          tileWidth,
          tileHeight,
        );
      }
    }

    this.player.draw(this.width / 2, this.height / 2);
    this.monsters.mobs.forEach((mob) => {
      const x =
        this.width / 2 +
        ((mob.x - this.player.x) * this.width) / this.cellSize;
      const y =
        this.height / 2 +
        ((mob.y - this.player.y) * this.height) / this.cellSize;
      mob.draw(x, y);
    });
    for (let i = 0; i < this.projectiles.length; i++) {
      const projectile = this.projectiles[i];
      const x =
        this.width / 2 +
        ((projectile.x - this.player.x) * this.width) / this.cellSize;
      const y =
        this.height / 2 +
        ((projectile.y - this.player.y) * this.height) /
          this.cellSize;
      projectile.draw(x, y);
    }
    this.texture.needsUpdate = true;
  }
}

class Ui {
  constructor() {
    this.container = document.createElement('div');
    this.container.className = 'game-ui';
    document.body.appendChild(this.container);

    const bottomLeft = document.createElement('div');
    bottomLeft.className = 'bottom-left';
    bottomLeft.innerHTML = '1 lvl, 1 exp, 0 gold';
    this.container.appendChild(bottomLeft);

    const bottomRight = document.createElement('div');
    bottomRight.className = 'bottom-right';
    bottomRight.innerHTML = 'attack';
    this.container.appendChild(bottomRight);
  }

  remove() {
    document.body.removeChild(this.container);
  }
}

class Tiled extends Example {
  constructor() {
    super();

    this.controls.dispose();
    this.camera.position.x = 0;
    this.camera.position.y = 14;
    this.camera.position.z = 0;
    this.camera.lookAt(new THREE.Vector3(0, 0, 0));
    this.controls = new GameControls(
      this.camera,
      this.renderer.domElement,
    );

    this.clock = new THREE.Clock();
    this.ui = new Ui();
    this.map = new Map();
    const geometryPlane = new THREE.BoxGeometry(20, 0.01, 20);
    const materialPlane = new THREE.MeshPhongMaterial({
      map: this.map.texture,
      flatShading: true,
      envMap: null,
    });
    const plane = new THREE.Mesh(geometryPlane, materialPlane);
    this.scene.add(plane);
    this.objects.push(plane);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.4);
    this.scene.add(this.ambientLight);
    this.objects.push(this.ambientLight);
    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.2,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    this.animating = true;
  }

  animate() {
    const now = Date.now();

    if (this.animating) {
      // Process player input
      let newPosition = {};
      let attack = 0;
      if (this.controls.keys.arrowUp) {
        newPosition.y = -0.1;
      }
      if (this.controls.keys.arrowDown) {
        newPosition.y = 0.1;
      }
      if (this.controls.keys.arrowLeft) {
        newPosition.x = -0.1;
      }
      if (this.controls.keys.arrowRight) {
        newPosition.x = 0.1;
      }
      if (this.controls.keys[' ']) {
        attack = 1;
      }

      if (newPosition.x || newPosition.y) {
        this.map.playerMove(newPosition);
      }
      if (attack) {
        this.map.playerAttack(now, attack);
      }
      this.map.update(now);
    }
  }

  remove() {
    this.ui.remove();
    super.remove();
  }
}

export default Tiled;
