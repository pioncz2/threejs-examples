class Mob {
  constructor(ctx, width, height, x, y) {
    console.log('spawn');
    this.hp = 100;
    this.x = x;
    this.y = y;
    this.radius = Math.min(width, height) / 2;
    this.size = 10;
    this.ctx = ctx;
  }
  update() {
    // this.hp -= 2;
  }
  draw(x, y) {
    this.ctx.beginPath();
    this.ctx.fillStyle = '#004070';
    this.ctx.arc(x, y, this.radius, 0, 2 * Math.PI);
    this.ctx.fill();
  }
}

class Monsters {
  constructor(ctx, width, height) {
    this.width = width;
    this.height = height;
    this.ctx = ctx;
    this.mobs = [];
    this.max = 1;
    this.last = 0;
    this.spawnDelay = 2000;
  }
  update(now) {
    // generate mobs if necessary
    if (
      this.mobs.length < this.max &&
      this.last + this.spawnDelay < now
    ) {
      this.mobs.push(
        new Mob(
          this.ctx,
          this.width,
          this.height,
          Math.random() * 4,
          Math.random() * 2,
        ),
      );
      this.last = now;
    }

    // update mobs / do their logic & draw
    for (let i = this.mobs.length - 1; i >= 0; i--) {
      const mob = this.mobs[i];
      mob.update();
      if (mob.hp < 0) {
        console.log('dead');
        this.mobs.splice(i, 1);
      }
    }
  }
}

export default Monsters;
