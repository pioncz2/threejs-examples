const TileData = [
  { id: 0, color: 'blue', walkable: false },
  { id: 1, color: 'green', walkable: true },
];

export default TileData;
