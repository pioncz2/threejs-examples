class Projectile {
  constructor({ x, y, rotation, ctx }) {
    this.radius = 4;
    this.x = x;
    this.y = y;
    this.rotation = rotation;
    this.hp = 100;
    this.ctx = ctx;
    this.color = 'red';
  }

  draw(offsetX, offsetY) {
    this.ctx.beginPath();
    this.ctx.fillStyle = this.color;
    this.ctx.arc(offsetX, offsetY, this.radius, 0, 2 * Math.PI);
    this.ctx.fill();
  }

  update() {}
}

export default Projectile;
