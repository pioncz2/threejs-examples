import Example from './example';

class BasicLightning extends Example {
  constructor() {
    super();

    this.camera.position.y = 5;
    this.camera.position.z = 5;

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

    const geometryCube = new THREE.BoxGeometry(1, 1, 1);
    const materialCube = new THREE.MeshPhongMaterial({
      color: '#2FA1D6',
      flatShading: true,
      envMap: null,
    });

    const geometryPlane = new THREE.BoxGeometry(20, 0.01, 20);
    const materialPlane = new THREE.MeshPhongMaterial({
      color: '#fff',
      flatShading: true,
      envMap: null,
    });

    const cube1 = new THREE.Mesh(geometryCube, materialCube);
    const cube2 = new THREE.Mesh(geometryCube, materialCube);
    const cube3 = new THREE.Mesh(geometryCube, materialCube);

    cube1.position.set(0, 0.5, 0);
    cube1.receiveShadow = true;
    cube1.castShadow = true;
    this.scene.add(cube1);
    this.objects.push(cube1);
    cube2.position.set(-2, 0.5, 0);
    cube2.receiveShadow = true;
    cube2.castShadow = true;
    this.scene.add(cube2);
    this.objects.push(cube2);
    cube3.position.set(2, 0.5, 0);
    cube3.receiveShadow = true;
    cube3.castShadow = true;
    this.scene.add(cube3);
    this.objects.push(cube3);

    const plane = new THREE.Mesh(geometryPlane, materialPlane);
    plane.receiveShadow = true;
    this.scene.add(plane);
    this.objects.push(plane);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
    this.scene.add(this.ambientLight);
    this.objects.push(this.ambientLight);
    const hemisphereLight = new THREE.HemisphereLight(
      '#ffffdd',
      '#080820',
      0.2,
    );
    this.scene.add(hemisphereLight);
    this.objects.push(hemisphereLight);

    const pointLight = new THREE.PointLight('#ffffdd', 0.7, 500);
    pointLight.position.set(0, 2, 2);
    pointLight.castShadow = true;
    this.scene.add(pointLight);
    this.objects.push(pointLight);

    const directionalLight = new THREE.DirectionalLight('#ffffdd', 0);
    directionalLight.castShadow = true;
    directionalLight.position.set(0, 2, 2);
    this.scene.add(directionalLight);
    this.objects.push(directionalLight);

    const spotLight = new THREE.SpotLight('#ffffdd', 0, 500);
    spotLight.castShadow = true;
    spotLight.position.set(0, 2, 2);
    this.scene.add(spotLight);
    this.objects.push(spotLight);

    this.movingLight = pointLight;
    this.movingAngle = 0;

    this.gui = new dat.GUI();
    const folder1 = this.gui.addFolder('AmbientLight');
    folder1.add(this.ambientLight, 'visible').listen();
    const folder2 = this.gui.addFolder('HemisphereLight');
    folder2.add(hemisphereLight, 'visible');
    this.gui
      .add({ 'Moving light': 'PointLight' }, 'Moving light', [
        'None',
        'PointLight',
        'DirectionalLight',
        'SpotLight',
      ])
      .onChange((value) => {
        this.movingLight.intensity = 0;
        ({
          None: () => {},
          PointLight: () => {
            this.movingLight = pointLight;
            this.movingLight.intensity = 0.7;
          },
          DirectionalLight: () => {
            this.movingLight = directionalLight;
            this.movingLight.intensity = 0.7;
          },
          SpotLight: () => {
            this.movingLight = spotLight;
            this.movingLight.intensity = 0.7;
          },
        }[value]());
      });
  }

  animate() {
    if (this.movingLight) {
      this.movingAngle += 1;
      this.movingAngle %= 360;
      this.movingLight.position.x =
        2 * Math.cos((this.movingAngle * Math.PI) / 180);
      this.movingLight.position.z =
        2 * Math.sin((this.movingAngle * Math.PI) / 180);
    }
  }

  remove() {
    super.remove();

    this.gui.destroy();
  }
}

export default BasicLightning;
