import Example from './example';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { BloomPass } from 'three/examples/jsm/postprocessing/BloomPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader.js';

class Nebula extends Example {
  constructor() {
    super();

    this.scene.fog = new THREE.FogExp2(0x03544e, 0.001);

    // lights

    let ambient = new THREE.AmbientLight(0x555555);
    this.scene.add(ambient);

    let directionalLight = new THREE.DirectionalLight(0xff8c19);
    directionalLight.position.set(0, 0, 1);
    this.scene.add(directionalLight);

    this.controls.reset();
    this.controls.dispose();
    this.controls = null;

    this.camera.rotation.x = 1.16;
    this.camera.rotation.y = -0.12;
    this.camera.rotation.z = 0.27;

    let orangeLight = new THREE.PointLight(0xcc6600, 50, 450, 1.7);
    orangeLight.position.set(200, 300, 100);
    this.scene.add(orangeLight);
    let redLight = new THREE.PointLight(0xd8547e, 50, 450, 1.7);
    redLight.position.set(100, 300, 100);
    this.scene.add(redLight);
    let blueLight = new THREE.PointLight(0x3677ac, 50, 450, 1.7);
    blueLight.position.set(300, 300, 200);
    this.scene.add(blueLight);

    // post-processing

    this.composer = new EffectComposer(this.renderer);

    const renderPass = new RenderPass(this.scene, this.camera);
    this.composer.addPass(renderPass);

    const bloomPass = new BloomPass(1, 20, 3);
    this.composer.addPass(bloomPass);

    var effectCopy = new ShaderPass(CopyShader);
    effectCopy.renderToScreen = true;
    this.composer.addPass(effectCopy);

    this.cloudParticles = [];

    // clouds

    let loader = new THREE.TextureLoader();
    loader.load('images/smoke-1.png', (texture) => {
      const cloudGeo = new THREE.PlaneBufferGeometry(500, 500);
      const cloudMaterial = new THREE.MeshLambertMaterial({
        map: texture,
        transparent: true,
      });

      for (let p = 0; p < 50; p++) {
        let cloud = new THREE.Mesh(cloudGeo, cloudMaterial);
        cloud.position.set(
          Math.random() * 800 - 400,
          500,
          Math.random() * 500 - 500,
        );
        cloud.rotation.x = 1.16;
        cloud.rotation.y = -0.12;
        cloud.rotation.z = Math.random() * 2 * Math.PI;
        cloud.material.opacity = 0.55;
        this.cloudParticles.push(cloud);
        this.scene.add(cloud);
      }
    });
  }

  animate() {
    this.cloudParticles.forEach((p) => {
      p.rotation.z -= 0.005;
    });
    this.composer.render(0.1);
    return false;
  }
}

export default Nebula;
