import Example from './example';
import * as THREE from 'three';
import * as DAT from 'three/examples/jsm/libs/dat.gui.module.js';
import { RoomEnvironment } from 'three/examples/jsm/environments/RoomEnvironment.js';

function getObjectsKeys(obj) {
  const keys = [];

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      keys.push(key);
    }
  }

  return keys;
}

const cubeTextureLoader = new THREE.CubeTextureLoader();
const textureLoader = new THREE.TextureLoader();

const envMaps = (function () {
  const path = '/cube/SwedishRoyalCastle/';
  const format = '.jpg';
  const urls = [
    path + 'px' + format,
    path + 'nx' + format,
    path + 'py' + format,
    path + 'ny' + format,
    path + 'pz' + format,
    path + 'nz' + format,
  ];

  const reflectionCube = cubeTextureLoader.load(urls);
  reflectionCube.format = THREE.RGBFormat;

  const refractionCube = cubeTextureLoader.load(urls);
  refractionCube.mapping = THREE.CubeRefractionMapping;
  refractionCube.format = THREE.RGBFormat;

  return {
    none: null,
    reflection: reflectionCube,
    refraction: refractionCube,
  };
})();

const diffuseMaps = (function () {
  const bricks = textureLoader.load('/images/brick_diffuse.jpg');
  bricks.wrapS = THREE.RepeatWrapping;
  bricks.wrapT = THREE.RepeatWrapping;
  bricks.repeat.set(9, 1);

  return {
    none: null,
    bricks: bricks,
  };
})();

const roughnessMaps = (function () {
  const bricks = textureLoader.load('/images/brick_roughness.jpg');
  bricks.wrapT = THREE.RepeatWrapping;
  bricks.wrapS = THREE.RepeatWrapping;
  bricks.repeat.set(9, 1);

  return {
    none: null,
    bricks: bricks,
  };
})();

const matcaps = (function () {
  return {
    none: null,
    porcelainWhite: textureLoader.load(
      '/images/matcap-porcelain-white.jpg',
    ),
  };
})();

const alphaMaps = (function () {
  const fibers = textureLoader.load('/images/alphaMap.jpg');
  fibers.wrapT = THREE.RepeatWrapping;
  fibers.wrapS = THREE.RepeatWrapping;
  fibers.repeat.set(9, 1);

  return {
    none: null,
    fibers: fibers,
  };
})();

const gradientMaps = (function () {
  const threeTone = textureLoader.load('/images/threeTone.jpg');
  threeTone.minFilter = THREE.NearestFilter;
  threeTone.magFilter = THREE.NearestFilter;

  const fiveTone = textureLoader.load('/images/fiveTone.jpg');
  fiveTone.minFilter = THREE.NearestFilter;
  fiveTone.magFilter = THREE.NearestFilter;

  return {
    none: null,
    threeTone: threeTone,
    fiveTone: fiveTone,
  };
})();

function generateVertexColors(geometry) {
  const positionAttribute = geometry.attributes.position;
  const colors = [];
  const color = new THREE.Color();

  for (let i = 0, il = positionAttribute.count; i < il; i++) {
    color.setHSL((i / il) * Math.random(), 0.5, 0.5);
    colors.push(color.r, color.g, color.b);
  }
  geometry.setAttribute(
    'color',
    new THREE.Float32BufferAttribute(colors, 3),
  );
}

const envMapKeys = getObjectsKeys(envMaps);
const diffuseMapKeys = getObjectsKeys(diffuseMaps);
const roughnessMapKeys = getObjectsKeys(roughnessMaps);
const alphaMapKeys = getObjectsKeys(alphaMaps);

class Standardmaterial extends Example {
  constructor() {
    super();

    this.renderer.outputEncoding = THREE.sRGBEncoding;

    const pmremGenerator = new THREE.PMREMGenerator(this.renderer);
    this.scene.environment = pmremGenerator.fromScene(
      new RoomEnvironment(),
      0.04,
    ).texture;

    this.camera.position.set(0, 0, 35);

    // Background
    const bgMaterials = [
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/px.jpg'),
        side: THREE.DoubleSide,
      }),
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/nx.jpg'),
        side: THREE.DoubleSide,
      }),
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/py.jpg'),
        side: THREE.DoubleSide,
      }),
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/ny.jpg'),
        side: THREE.DoubleSide,
      }),
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/pz.jpg'),
        side: THREE.DoubleSide,
      }),
      new THREE.MeshBasicMaterial({
        map: this.loader.load('/cube/SwedishRoyalCastle/nz.jpg'),
        side: THREE.DoubleSide,
      }),
    ];
    const bg = new THREE.Mesh(
      new THREE.BoxGeometry(300, 300, 300),
      bgMaterials,
    );
    this.scene.add(bg);

    // Lights
    const ambientLight = new THREE.AmbientLight(0x000000);
    this.scene.add(ambientLight);

    const light1 = new THREE.PointLight(0xffffff, 1, 0);
    light1.position.set(0, 200, 0);
    this.scene.add(light1);

    const light2 = new THREE.PointLight(0xffffff, 1, 0);
    light2.position.set(60, 0, 80);
    this.scene.add(light2);

    const light3 = new THREE.PointLight(0xffffff, 1, 0);
    light3.position.set(-100, -200, -100);
    this.scene.add(light3);

    const geometry = new THREE.TorusKnotGeometry(
      10,
      3,
      200,
      32,
    ).toNonIndexed();

    generateVertexColors(geometry);

    const mesh = new THREE.Mesh(geometry);
    mesh.material = new THREE.MeshStandardMaterial({
      color: 0x049ef4,
      transparent: true,
    });

    this.scene.add(mesh);

    const data = {
      color: mesh.material.color.getHex(),
      emissive: mesh.material.emissive.getHex(),
      envMaps: envMapKeys[0],
      map: diffuseMapKeys[0],
      roughnessMap: roughnessMapKeys[0],
      alphaMap: alphaMapKeys[0],
    };

    this.gui = new DAT.GUI();

    const folder3 = this.gui.addFolder('Presets');
    var obj = {
      stone: function () {
        console.log('clicked');
      },
      wood: function () {
        console.log('clicked');
      },
      metal: function () {
        console.log('clicked');
      },
      jelly: function () {
        mesh.material.roughness = 0.6;
        mesh.material.metalness = 0.2;
        mesh.material.envMapIntensity = 5;
        mesh.material.opacity = 0.4;
        light1.visible = true;
        light2.visible = true;
        light3.visible = true;
        mesh.material.envMap = envMaps.reflection;
        data.envMaps = envMapKeys[1];
        mesh.material.shininess = 2;
        mesh.material.opacity = 0.9;
        mesh.material.emissive.setHex(0x000000);
        data.emissive = 0x000000;
      },
      glass: function () {
        mesh.material.roughness = 0;
        mesh.material.envMapIntensity = 5;
        mesh.material.opacity = 0.4;
        light1.visible = true;
        light2.visible = true;
        light3.visible = true;
        mesh.material.envMap = envMaps.reflection;
        data.envMaps = envMapKeys[1];
        mesh.material.shininess = 2;
        mesh.material.opacity = 0.8;
        mesh.material.emissive.setHex(0x434343);
        data.emissive = 0x434343;
      },
    };
    folder3.add(obj, 'stone').name('Stone');
    folder3.add(obj, 'wood').name('Wood');
    folder3.add(obj, 'metal').name('Metal');
    folder3.add(obj, 'jelly').name('Jelly');
    folder3.add(obj, 'glass').name('Glass');

    const folder1 = this.gui.addFolder('Lights');
    folder1.add(light1, 'visible').name('PointLight 1').listen();
    folder1.add(light2, 'visible').name('PointLight 2').listen();
    folder1.add(light3, 'visible').name('PointLight 3').listen();
    folder1
      .add(ambientLight, 'visible')
      .name('Ambient light visible');
    folder1
      .addColor(ambientLight, 'color')
      .name('Ambient light color');

    const folder = this.gui.addFolder('MeshStandardMaterial');
    folder
      .addColor(data, 'color')
      .onChange(handleColorChange(mesh.material.color));
    folder
      .addColor(data, 'emissive')
      .onChange(handleColorChange(mesh.material.emissive))
      .listen();
    folder.add(mesh.material, 'opacity', 0, 1).listen();
    folder.add(mesh.material, 'roughness', 0, 1).listen();
    folder.add(mesh.material, 'metalness', 0, 1).listen();
    folder.add(mesh.material, 'wireframe');
    folder
      .add(mesh.material, 'flatShading')
      .onChange(needsUpdate(mesh.material, geometry));
    folder
      .add(mesh.material, 'vertexColors')
      .onChange(needsUpdate(mesh.material, geometry));
    folder.add(mesh.material, 'fog');
    folder
      .add(data, 'envMaps', envMapKeys)
      .onChange(updateTexture(mesh.material, 'envMap', envMaps))
      .listen();
    folder.add(mesh.material, 'envMapIntensity', 0, 10).listen();
    folder
      .add(data, 'map', diffuseMapKeys)
      .onChange(updateTexture(mesh.material, 'map', diffuseMaps));
    folder
      .add(data, 'roughnessMap', roughnessMapKeys)
      .onChange(
        updateTexture(mesh.material, 'roughnessMap', roughnessMaps),
      );
    folder
      .add(data, 'alphaMap', alphaMapKeys)
      .onChange(updateTexture(mesh.material, 'alphaMap', alphaMaps));

    function needsUpdate(material, geometry) {
      return function () {
        material.vertexColors = material.vertexColors;
        material.side = parseInt(material.side); //Ensure number
        material.needsUpdate = true;
        geometry.attributes.position.needsUpdate = true;
        geometry.attributes.normal.needsUpdate = true;
      };
    }

    function handleColorChange(color) {
      return function (value) {
        if (typeof value === 'string') {
          value = value.replace('#', '0x');
        }
        color.setHex(value);
      };
    }

    function updateTexture(material, materialKey, textures) {
      return function (key) {
        material[materialKey] = textures[key];
        material.needsUpdate = true;
      };
    }
  }

  animate() {}

  remove() {
    super.remove();

    this.gui.destroy();
  }
}

export default Standardmaterial;
