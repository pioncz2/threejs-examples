import Example from './example';

class Basic extends Example {
  constructor() {
    super();

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    const cube = new THREE.Mesh(geometry, material);
    this.objects.push(cube);
    this.scene.add(cube);
  }

  animate() {
    if (this.objects.length) {
      this.objects[0].rotation.x += 0.01;
      this.objects[0].rotation.y += 0.01;
    }
  }
}

export default Basic;
