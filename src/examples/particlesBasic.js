import Example from './example';

const ParticleCount = 1800;

class ParticlesBasic extends Example {
  constructor() {
    super();

    this.camera.position.z = 5;
    this.renderer.domElement.style.background =
      'radial-gradient(ellipse at center 90%, hsla(180, 100%, 50%, 1) 0%, #001d14 ';

    this.geometry = new THREE.BufferGeometry();
    const vertices = [];
    this.velocities = [0.1, 0.3, 0.6];

    const pMaterial = new THREE.PointsMaterial({
      color: new THREE.Color('hsl(180, 100%, 50%)'),
      size: 4,
      map: this.loader.load('/images/particle2.png'),
      blending: THREE.AdditiveBlending,
      transparent: true,
    });

    // create the particles
    for (let p = 0; p < ParticleCount; p++) {
      // create a particle with random
      // position values, -250 -> 250
      const pX = Math.random() * 500 - 250;
      const pY = Math.random() * 500 - 250;
      const pZ = Math.random() * 500 - 250;

      vertices.push(pX, pY, pZ);
    }

    this.geometry.setAttribute(
      'position',
      new THREE.Float32BufferAttribute(vertices, 3),
    );

    // create the particle system
    this.particleSystem = new THREE.Points(this.geometry, pMaterial);

    // add it to the scene
    this.scene.add(this.particleSystem);
    this.objects = [this.particleSystem];

    this.startTime = Date.now();
    this.animating = true;
  }

  animate() {
    if (this.animating) {
      let pCount = ParticleCount;
      const positionsArray =
        this.particleSystem.geometry.attributes.position.array;

      while (pCount--) {
        const pIndex = pCount * 3;
        let pX = positionsArray[pIndex];
        let pY = positionsArray[pIndex + 1];

        // check if we need to reset y
        if (pY < -200) {
          pY = 200;
        }

        // calc new position
        pX -= this.velocities[pCount % this.velocities.length] * 0.25;
        pY -= this.velocities[pCount % this.velocities.length];

        positionsArray[pIndex] = pX;
        positionsArray[pIndex + 1] = pY;
      }
      // flag to the particle system that we've changed its vertices.
      this.particleSystem.geometry.attributes.position.needsUpdate = true;
    }
  }
}

export default ParticlesBasic;
