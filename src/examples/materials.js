import Example from './example';

class Materials extends Example {
  constructor() {
    super();

    this.scene.fog = null;

    this.camera.position.set(0, 0, 1000);
    this.camera.fov = 45;
    this.camera.near = 1;
    this.camera.far = 2000;
    this.camera.lookAt(new THREE.Vector3(0, 0, 0));
    this.camera.updateProjectionMatrix();

    this.objects = [];

    const generateTexture = () => {
      const canvas = document.createElement('canvas');
      canvas.width = 256;
      canvas.height = 256;

      const context = canvas.getContext('2d');
      const image = context.getImageData(0, 0, 256, 256);

      let x = 0,
        y = 0;

      for (
        let i = 0, j = 0, l = image.data.length;
        i < l;
        i += 4, j++
      ) {
        x = j % 256;
        y = x === 0 ? y + 1 : y;

        image.data[i] = 255;
        image.data[i + 1] = 255;
        image.data[i + 2] = 255;
        image.data[i + 3] = Math.floor(x ^ y);
      }

      context.putImageData(image, 0, 0);

      return canvas;
    };

    const addMesh = (geometry, material, x, y) => {
      const mesh = new THREE.Mesh(geometry, material);

      mesh.position.x = x * 200 - 400;
      mesh.position.y = y * 200 - 300;

      mesh.rotation.x = Math.random() * 200 - 100;
      mesh.rotation.y = Math.random() * 200 - 100;
      mesh.rotation.z = Math.random() * 200 - 100;

      this.objects.push(mesh);

      this.scene.add(mesh);
    };

    // Materials
    this.materials = [[], [], [], [], []];

    const texture = new THREE.Texture(generateTexture());
    texture.needsUpdate = true;

    this.materials[0].push(
      new THREE.MeshBasicMaterial({
        map: texture,
        transparent: true,
      }),
    );
    this.materials[0].push(
      new THREE.MeshBasicMaterial({
        color: 0xffaa00,
        wireframe: true,
      }),
    );
    this.materials[0].push(
      new THREE.MeshBasicMaterial({
        color: 0xffaa00,
        transparent: true,
        blending: THREE.AdditiveBlending,
      }),
    );

    this.materials[1].push(new THREE.MeshNormalMaterial());
    this.materials[1].push(
      new THREE.MeshNormalMaterial({ flatShading: true }),
    );

    // Non shiny
    this.materials[2].push(
      new THREE.MeshLambertMaterial({
        map: texture,
        transparent: true,
      }),
    );
    this.materials[2].push(
      new THREE.MeshLambertMaterial({
        color: 0xdddddd,
        emissive: 0xff0000,
        emissiveIntensity: 0.1,
        transparent: true,
        opacity: 0.85,
      }),
    );
    this.materials[2].push(
      new THREE.MeshLambertMaterial({
        color: 0xdddddd,
        emissive: 0xff0000,
        emissiveIntensity: 1.0,
        transparent: true,
        opacity: 0.85,
      }),
    );
    this.materials[2].push(
      new THREE.MeshLambertMaterial({
        color: 0xdddddd,
        emissive: 0xff0000,
        transparent: true,
        opacity: 0.85,
      }),
    );

    // Shiny
    this.materials[3].push(
      new THREE.MeshPhongMaterial({
        color: 0xdddddd,
        specular: 0xff0000,
        shininess: 30,
        map: texture,
        transparent: true,
      }),
    );
    this.materials[3].push(
      new THREE.MeshPhongMaterial({
        color: 0xdddddd,
        specular: 0xff0000,
        shininess: 10,
        transparent: true,
      }),
    );
    this.materials[3].push(
      new THREE.MeshPhongMaterial({
        color: 0xdddddd,
        specular: 0xff0000,
        shininess: 90,
        transparent: true,
      }),
    );

    this.materials[3].push(
      new THREE.MeshPhongMaterial({
        color: 0xdddddd,
        specular: 0xff0000,
        shininess: 10,
        transparent: true,
        map: this.loader.load('images/texture1.jpg'),
        normalMap: this.loader.load('images/normalMap1.jpg'),
      }),
    );

    this.materials[3].push(
      new THREE.MeshPhongMaterial({
        color: 0x000000,
        specular: 0x666666,
        emissive: 0xff0000,
        shininess: 10,
        opacity: 0.9,
        transparent: true,
      }),
    );

    // Standard: shiny + non shiny

    this.materials[4].push(new THREE.MeshDepthMaterial());

    // Spheres geometry

    const geometry = new THREE.SphereGeometry(70, 32, 16);

    for (let i = 0, l = this.materials.length; i < l; i++) {
      for (let j = 0, l2 = this.materials[i].length; j < l2; j++) {
        addMesh(geometry, this.materials[i][j], i, j);
      }
    }

    // Lights

    this.scene.add(new THREE.AmbientLight(0x111111));

    const directionalLight = new THREE.DirectionalLight(
      0xffffff,
      0.1,
    );
    const pos = this.camera.position;
    directionalLight.position.set(pos.x, -400, pos.z);
    directionalLight.lookAt(this.scene.position);
    this.scene.add(directionalLight);

    const directionalLight2 = new THREE.DirectionalLight(
      0xffffff,
      0.1,
    );
    directionalLight2.position.set(pos.x, 400, pos.z);
    directionalLight2.lookAt(this.scene.position);
    this.scene.add(directionalLight2);

    this.pointLight1 = new THREE.PointLight(0xffffff, 1);
    this.pointLight1.add(
      new THREE.Mesh(
        new THREE.SphereGeometry(4, 8, 8),
        new THREE.MeshBasicMaterial({ color: 0xffffff }),
      ),
    );
    this.pointLight1.position.z = 100;
    this.scene.add(this.pointLight1);

    this.pointLight2 = new THREE.PointLight(0xffffff, 1);
    this.pointLight2.add(
      new THREE.Mesh(
        new THREE.SphereGeometry(4, 8, 8),
        new THREE.MeshBasicMaterial({ color: 0xffffff }),
      ),
    );
    this.pointLight2.position.z = 100;
    this.scene.add(this.pointLight2);
  }

  animate() {
    const timer = 0.0001 * Date.now();

    this.camera.lookAt(this.scene.position);

    for (let i = 0, l = this.objects.length; i < l; i++) {
      const object = this.objects[i];

      object.rotation.x += 0.01;
      object.rotation.y += 0.005;
    }

    this.materials[2][this.materials[2].length - 1].emissive.setHSL(
      0.54,
      1,
      0.35 * (0.5 + 0.5 * Math.sin(35 * timer)),
    );
    this.materials[3][this.materials[3].length - 1].emissive.setHSL(
      0.04,
      1,
      0.35 * (0.5 + 0.5 * Math.sin(35 * timer)),
    );

    this.pointLight1.position.x = Math.sin(timer * 5) * 200;
    this.pointLight1.position.y = Math.cos(timer * 5) * 200;

    this.pointLight2.position.x = Math.sin(timer * 5 + Math.PI) * 200;
    this.pointLight2.position.y = Math.cos(timer * 5 + Math.PI) * 200;
  }
}

export default Materials;
