export const degreesToRadians = (degrees) =>
  (degrees * Math.PI) / 180;

export const pointInDirection = (point, direction, radius) => {
  const x = point.x + Math.cos(direction) * radius;
  const z = point.z + Math.sin(direction) * radius;

  return { x, z };
};

export const angleBetweenPoints = (point1, point2) =>
  Math.atan2(point2.z - point1.z, point2.x - point1.x);

export const distanceBetweenPoints = (point1, point2) => {
  const a = point2.x - point1.x;
  const b = point2.z - point1.z;

  return Math.sqrt(a * a + b * b);
};

export const getObjectsInRadius = (originPosition, objects, radius) =>
  objects.filter(
    (o) =>
      distanceBetweenPoints(originPosition, o.$.position) < radius,
  );
