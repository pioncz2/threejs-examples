export const mapMoveDirectionToTextureScale = (radians) =>
  Math.abs(radians) <= 1.58 || Math.abs(radians) >= 5.497 ? 1 : -1;

export const mapMoveDirectionToTextureOrientation = (radians) =>
  Math.abs(radians) <= 1.58 || Math.abs(radians) >= 5.497
    ? 'left'
    : 'right';

export const getMoveStepForSpeed = (speed) => 0.001 * speed;
