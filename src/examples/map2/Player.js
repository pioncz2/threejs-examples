import Creature, { States } from './Creature';

class Player extends Creature {
  constructor({ debug, maxAnisotropy }) {
    super({ debug, maxAnisotropy, attack: 15, defence: 4 });

    this.$.position.set(0, 0.5, 0);

    const pointLight = new THREE.PointLight('#F9DDFF', 0.5, 8, 2);
    pointLight.position.set(0, 0.8, 0);
    this.$.add(pointLight);

    this.state = States.walking;
    this.shouldTriggerAttack = false;
    this.attackTriggered = false;
    this.attackRadius = 1.3;
  }
  setState(newState) {
    if (
      newState === this.state ||
      (this.state === States.attack &&
        this.sprite.animationName === 'attack')
    )
      return;

    if (newState === States.idle) {
      this.sprite.playContinuous('idle');
    } else if (newState === States.attack) {
      this.sprite.playOnce('attack');
    } else if (newState === States.walking) {
      this.sprite.playContinuous('run');
    }

    if (newState !== States.attack) {
      this.shouldTriggerAttack = false;
      this.attackTriggered = false;
    }

    this.state = newState;
  }
  animate(delta) {
    super.animate(delta);
    this.sprite.animate(delta);

    if (
      this.sprite.animationName === 'attack' &&
      this.sprite.animationFrame >=
        Math.round(0.75 * this.sprite.animationFrames.length) &&
      !this.shouldTriggerAttack
    ) {
      this.shouldTriggerAttack = true;
    }
  }
}

export default Player;
