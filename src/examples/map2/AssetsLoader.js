export const AssetNames = {
  Nightborne: 'nightborne',
};

export const FontNames = {
  ExpressionPro: 'ExpressionPro',
};

class AssetsLoader {
  constructor() {
    let resolve = null;
    this.load = new Promise((r) => (resolve = r));

    this.assets = {};

    const fontsPromise = new Promise((resolve) => {
      const expressionProFont = new FontFace(
        'ExpressionPro',
        'url(fonts/ExpressionPro.otf)',
      );
      expressionProFont.load().then((font) => {
        document.fonts.add(font);
        resolve();
      });
    });

    const spritesPromise = new Promise((resolve) => {
      fetch('sprites/nightborne.json')
        .then((response) => response.json())
        .then((spriteData) => {
          this.assets[AssetNames.Nightborne] = spriteData;
          resolve();
        });
    });

    Promise.all([fontsPromise, spritesPromise]).then(() => {
      resolve();
    });
  }
}

export default AssetsLoader;
