import Creature, { States } from './Creature';
import {
  distanceBetweenPoints,
  angleBetweenPoints,
} from './utils/math';
import {
  mapMoveDirectionToTextureScale,
  mapMoveDirectionToTextureOrientation,
  getMoveStepForSpeed,
} from './utils/creature';

const PlayerNoticeDistance = 5;
const PlayerAttackDistance = 1.3;

// Potwor ma miec 3 punkty pomiedzy ktorymi sie porusza
// Zaczyna od stanu idle, czeka 3 sekundy, idzie do nastepnego punktu i przechodzi w idle
// Gdy zauwazy gracza, przechodzi w stan attack
// Idzie w strone gracza, gdy jest bardzo blisko, to animuje attack
// animate() zwraca CreatureAction

class Enemy extends Creature {
  constructor({ debug = false, maxAnisotropy, color, position }) {
    super({
      debug,
      maxAnisotropy,
      color,
      speed: 20,
      hp: 20,
      attack: 10,
      defence: 1,
    });

    this.initialPosition = position;
    this.$.position.set(position.x, position.y, position.z);
    this.name = 'Enemy 1';
    this.state = States.idle;
    this.enemy = true;
    this.shouldTriggerAttack = false;
    this.attackTriggered = false;
    this.attackRadius = 1.3;
  }
  setState(newState) {
    if (newState === this.state) return;

    if (newState === States.idle) {
      this.sprite.playContinuous('idle');
    } else if (newState === States.walking) {
      this.sprite.playContinuous('run');
    } else if (newState === States.chase) {
      this.sprite.playContinuous('run');
    } else if (newState === States.attack) {
      this.sprite.playContinuous('attack');
    }

    this.state = newState;
  }
  animate(delta, map) {
    super.animate(delta);

    const playerPosition = map.getPosition();
    const distanceToPlayer = distanceBetweenPoints(
      playerPosition,
      this.$.position,
    );

    if (
      distanceToPlayer < PlayerNoticeDistance &&
      this.state === States.idle
    ) {
      this.setState(States.chase);
    } else if (
      distanceToPlayer > PlayerNoticeDistance &&
      (this.state === States.chase || this.state === States.attack)
    ) {
      this.setState(States.walking);
    } else if (
      distanceToPlayer < PlayerNoticeDistance &&
      distanceToPlayer > PlayerAttackDistance &&
      this.state === States.attack
    ) {
      this.attackTriggered = false;
      this.shouldTriggerAttack = false;
      this.setState(States.chase);
    }

    if (this.state === States.chase) {
      const startPosition = this.$.position;
      const playerPosition = map.getPosition();
      const distance = distanceBetweenPoints(
        startPosition,
        playerPosition,
      );

      if (distance < PlayerAttackDistance) {
        this.setState(States.attack);
      } else {
        const radians = angleBetweenPoints(
          startPosition,
          playerPosition,
        );

        this.moveInDirection(radians);
        this.turn(mapMoveDirectionToTextureOrientation(radians));
      }
    }
    if (this.state === States.walking) {
      const startPosition = this.$.position;
      const endPosition = this.initialPosition;
      const distance = distanceBetweenPoints(
        startPosition,
        endPosition,
      );
      const moveStep = getMoveStepForSpeed(this.speed);

      if (distance < moveStep) {
        this.$.position.x = endPosition.x;
        this.$.position.z = endPosition.z;
        this.setState(States.idle);
      } else {
        const radians = angleBetweenPoints(
          startPosition,
          endPosition,
        );

        this.moveInDirection(radians);
        this.turn(mapMoveDirectionToTextureOrientation(radians));
      }
    }
    if (
      this.sprite.animationName === 'attack' &&
      this.sprite.animationFrame >=
        Math.round(0.75 * this.sprite.animationFrames.length) &&
      !this.shouldTriggerAttack &&
      !this.attackTriggered
    ) {
      this.shouldTriggerAttack = true;
    }
    if (
      this.sprite.animationName === 'attack' &&
      this.sprite.animationFrame ===
        this.sprite.animationFrames.length - 1
    ) {
      this.shouldTriggerAttack = false;
      this.attackTriggered = false;
    }
  }
}

export default Enemy;
