import Sprite from './Sprite.js';

class TilesetManager {
  constructor(maxAnisotropy) {
    this.loadingPromise = new Promise((resolve, reject) => {
      this.loadingPromiseResolve = resolve;
    });
    this.maxAnisotropy = maxAnisotropy;
    this.tilesets = [];
  }
  load(tileObjects) {
    this.tilesets = [...tileObjects];

    const loader = new THREE.ImageLoader();
    const loadPromises = [];
    this.tilesets.forEach((tileset) => {
      loadPromises.push(
        new Promise((resolve, reject) => {
          loader.load(
            tileset.assetUrl,
            (image) => {
              tileset.asset = image;
              resolve();
            },
            undefined,
            function () {
              console.error('Error while loading tileset asset');
              reject();
            },
          );
        }),
      );
    });

    Promise.all(loadPromises).then(() => {
      this.tilesets.forEach((tileset) => {
        tileset.tiles.forEach((tile) => {
          if (tile.frame) {
            const { frame } = tile;
            const canvas = document.createElement('canvas');
            canvas.width = frame.w;
            canvas.height = frame.h;
            const ctx = canvas.getContext('2d');
            ctx.imageSmoothingEnabled = false;
            ctx.drawImage(
              tileset.asset,
              frame.x,
              frame.y,
              frame.w,
              frame.h,
              0,
              0,
              frame.w,
              frame.h,
            );
            const texture = new THREE.CanvasTexture(canvas);
            texture.anisotropy = this.maxAnisotropy;
            texture.magFilter = THREE.NearestFilter;
            texture.minFilter = THREE.LinearMipMapLinearFilter;
            tile.texture = texture;
          }
          if (tile?.frames?.length) {
            const canvas = document.createElement('canvas');
            canvas.width = tile.frames[0].w;
            canvas.height = tile.frames[0].h;
            const ctx = canvas.getContext('2d');
            ctx.imageSmoothingEnabled = false;
            const texture = new THREE.CanvasTexture(canvas);
            texture.anisotropy = this.maxAnisotropy;
            texture.magFilter = THREE.NearestFilter;
            texture.minFilter = THREE.LinearMipMapLinearFilter;
            const sprite = new Sprite(canvas, texture, 10);
            sprite.setAssetImage(tileset.asset);
            sprite.setAnimations({
              animations: [
                {
                  name: 'main',
                  frames: tile.frames.map((frame) => ({
                    x: frame.x,
                    y: frame.y,
                    width: frame.w,
                    height: frame.h,
                  })),
                },
              ],
            });
            sprite.playOnce('main');
            tile.sprite = sprite;
            tile.texture = texture;
          }
        });
      });

      this.loadingPromiseResolve();
    });

    return this.loadingPromise;
  }
  findTile(id) {
    let returnTile;

    for (let tilesetIdx in this.tilesets) {
      const tileset = this.tilesets[tilesetIdx];
      returnTile = tileset.tiles.find((tile) => tile.id === id);

      if (returnTile) {
        return returnTile;
      }
    }
  }
}

export default TilesetManager;
