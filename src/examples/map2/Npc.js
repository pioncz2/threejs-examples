import Creature, { States } from './Creature';
import {
  angleBetweenPoints,
  distanceBetweenPoints,
} from './utils/math';
import {
  mapMoveDirectionToTextureScale,
  getMoveStepForSpeed,
} from './utils/creature';

const EventRadius = 5;
const DialogTypes = {
  greetings: 'greetings',
  farewells: 'farewells',
};

class Npc extends Creature {
  constructor({ debug = false, maxAnisotropy, color, position }) {
    super({
      debug,
      maxAnisotropy,
      color,
      speed: 20,
      creatureEffects: false,
    });

    this.name = 'Npc 1';
    this.state = States.idle;
    this.dialogs = {
      greetings: [
        (name) => `Hi, I'm ${this.name}. Supp?`,
        'Hey.',
        'Hello!',
      ],
      farewells: ['Goodbye', 'Farewell'],
    };

    this.$.position.set(position.x, position.y, position.z);

    this.walkPositions = [
      { x: position.x, z: position.z },
      { x: position.x, z: position.z + 4 },
      { x: position.x + 10, z: position.z + 4 },
    ];
    this.lastPosition = 0;
    this.targetPosition = 1;
  }

  getDialog(type = 'greetings') {
    const messages = this.dialogs[type];
    const random = Math.floor(Math.random() * messages.length);
    const message = messages[random];
    if (message instanceof Function) {
      return message(this.name);
    } else {
      return message;
    }
  }
  setState(newState) {
    if (newState === this.state) return;

    if (newState === States.talking) {
      const greeting = this.getDialog(DialogTypes.greetings);
      console.log(greeting);
      this.sprite.playContinuous('idle');
    }
    if (this.state === States.talking) {
      this.$.remove(this.dialogBubble);
      const greeting = this.getDialog(DialogTypes.farewells);
      console.log(greeting);
      this.sprite.playContinuous('idle');
    }
    if (newState === States.walking) {
      this.sprite.playContinuous('run');
    }

    this.state = newState;
  }
  animate(delta, map) {
    const isPlayerInRadius = map.isPlayerInRadius(
      this.$.position,
      EventRadius,
    );

    if (isPlayerInRadius) {
      this.setState(States.talking);
    } else {
      this.setState(States.walking);
    }

    if (this.state === States.walking) {
      const startPosition = this.$.position;
      const endPosition = this.walkPositions[this.targetPosition];
      const distance = distanceBetweenPoints(
        startPosition,
        endPosition,
      );
      const moveStep = getMoveStepForSpeed(this.speed);

      if (distance < moveStep) {
        this.$.position.x = endPosition.x;
        this.$.position.z = endPosition.z;
        this.lastPosition = this.targetPosition;
        this.targetPosition++;
        if (this.targetPosition >= this.walkPositions.length) {
          this.targetPosition = 0;
        }
      } else {
        const radians = angleBetweenPoints(
          startPosition,
          endPosition,
        );

        this.moveInDirection(radians);
        this.sprite$.scale.x =
          mapMoveDirectionToTextureScale(radians);
      }
    }
  }
}

export default Npc;
