const rand = (min, max) => Math.random() * (max - min) + min;

const makeArray = (length) => Array.from({ length }, (v, i) => i);

const generateMapFragment = ({
  size,
  offset,
  ids,
  r,
  fillFunction,
}) =>
  makeArray(size.x)
    .map((x) =>
      makeArray(size.z).map((z) => ({
        x: x + (offset?.x || 0),
        y: size.y || 0,
        z: z + (offset?.z || 0),
        id: ids[Math.floor(rand(0, ids.length))],
        ...(r ? { r } : {}),
        ...(fillFunction
          ? fillFunction(ids, x, z, size, offset)
          : {}),
      })),
    )
    .flat()
    .filter((mapPos) => mapPos.id || mapPos.id === 0);

const generateMapFromFragments = (fragments) => {
  return [
    ...fragments.map((fragment) => generateMapFragment(fragment)),
  ].flat();
};

const generateColumn = (x, z, id, width) => {
  const pos = [];
  const width2 = width / 2;
  pos.push({
    x,
    y: 0,
    z,
    id,
  });
  pos.push({
    x: x + width2,
    y: 0,
    z: z - width2,
    r: 90,
    id,
  });
  pos.push({
    x,
    y: 0,
    z: z + width,
    id,
  });
  pos.push({
    x: x + 3 * width2,
    y: 0,
    z: z - width2,
    r: 90,
    id,
  });
  return pos;
};

let mapPos = generateMapFromFragments([
  // Floors
  {
    size: { x: 30, z: 10 },
    ids: [2, 3, 4],
  },
  // Grass isle
  {
    size: { x: 6, z: 6 },
    offset: { x: 3, z: 1 },
    ids: [5, 6, 7, 8, 9, 10, 11, 12, 13],
    fillFunction: (ids, x, z, size) => {
      let id = ids[4];
      if (x === 0 && z === 0) {
        id = ids[0];
      }
      if (x > 0 && x < size.x - 1 && z === 0) {
        id = ids[1];
      }
      if (x === size.x - 1 && z === 0) {
        id = ids[2];
      }
      if (z > 0 && z < size.z - 1 && x === 0) {
        id = ids[3];
      }
      if (z < size.z - 1 && z > 0 && x === size.x - 1) {
        id = ids[5];
      }
      if (z === size.z - 1 && x === 0) {
        id = ids[6];
      }
      if (z === size.z - 1 && x > 0 && x < size.x - 1) {
        id = ids[7];
      }
      if (z === size.z - 1 && x === size.x - 1) {
        id = ids[8];
      }

      return { id };
    },
  },
  // Catacomb tiles
  // {
  //   size: { x: 1, z: 2 },
  //   offset: { x: 5, z: 5 },
  //   ids: ['f1', 'f2'],
  //   // fillFunction: (ids, x, z, size) => {
  //   // }
  // },
  // {
  //   size: { x: 1, z: 2 },
  //   offset: { x: 6, z: 5 },
  //   ids: ['f3'],
  //   // fillFunction: (ids, x, z, size) => {
  //   // }
  // },
  // Wall left
  {
    size: { x: 30, z: 1 },
    ids: ['c1'],
  },
  // Wall top
  {
    size: { x: 1, z: 10 },
    offset: { x: 30, z: 0 },
    ids: ['c1'],
    r: 90,
  },
  // Wall bottom
  {
    size: { x: 1, z: 10 },
    ids: ['c1'],
    r: 90,
  },
  // Wall right
  {
    size: { x: 30, z: 1 },
    offset: { z: 10 },
    ids: ['c1'],
  },
  // Hangings
  {
    size: { x: 30, z: 1 },
    offset: { z: 0.01 },
    ids: ['c2'],
    fillFunction: (ids, x) => ({ id: x % 5 === 4 ? ids[0] : null }),
  },
]);

// Columns
mapPos = [...mapPos, ...generateColumn(4, 2, 'c3', 0.5)];
mapPos = [...mapPos, ...generateColumn(9, 2, 'c3', 0.5)];
mapPos = [...mapPos, ...generateColumn(14, 2, 'c3', 0.5)];
mapPos = [...mapPos, ...generateColumn(19, 2, 'c3', 0.5)];
mapPos = [...mapPos, ...generateColumn(24, 2, 'c3', 0.5)];

export { mapPos };
