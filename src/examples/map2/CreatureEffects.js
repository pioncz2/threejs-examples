import { FontNames } from './AssetsLoader.js';

export const TextTypes = {
  Damage: 'damage',
};

class CreatureEffects {
  constructor({ maxAnisotropy }) {
    this.maxAnisotropy = maxAnisotropy;

    this.texts = [];

    this.size = 256;
    const geometry = new THREE.PlaneGeometry(1, 1);
    const canvas = document.createElement('canvas');
    canvas.width = this.size;
    canvas.height = this.size;
    this.ctx = canvas.getContext('2d');
    this.ctx.imageSmoothingEnabled = false;
    this.ctx.clearRect(0, 0, this.size, this.size);
    this.orientation = 'right';

    this.texture = new THREE.CanvasTexture(canvas);
    this.texture.anisotropy = this.maxAnisotropy;
    this.texture.magFilter = THREE.NearestFilter;
    this.texture.minFilter = THREE.LinearMipMapLinearFilter;
    const material = new THREE.MeshBasicMaterial({
      map: this.texture,
      transparent: true,
      opacity: 1.0,
      side: THREE.DoubleSide,
      depthTest: false,
    });
    this.$ = new THREE.Mesh(geometry, material);
  }
  turn(orientation) {
    this.orientation = orientation;
  }
  add(type, value) {
    if (this.texts.length) return;

    this.texts.push({
      type,
      value: value,
      lengthLeft: 1,
      length: 1,
      orientation: this.orientation,
    });
  }
  animate(delta) {
    if (!this.texts.length) return;

    this.ctx.clearRect(0, 0, this.size, this.size);

    for (let i = this.texts.length - 1; i >= 0; i--) {
      const text = this.texts[i];
      const progress = 1 - text.lengthLeft / text.length;

      if (text.lengthLeft <= 0) {
        this.texts.splice(i, 1);
      } else if (text.type === TextTypes.Damage) {
        const y = Math.round(80 + 140 * (1 - progress));
        const x = text.orientation === 'left' ? 10 : this.size - 10;

        this.ctx.textAlign =
          text.orientation === 'left' ? 'start' : 'end';
        this.ctx.font = `72px ${FontNames.ExpressionPro}`;
        this.ctx.fillStyle = '#6d0000';
        this.ctx.lineWidth = 8;
        this.ctx.strokeStyle = '#000000';
        this.ctx.strokeText(-text.value, x, y);
        this.ctx.fillText(-text.value, x, y);
      }

      text.lengthLeft -= delta;
    }
    this.texture.needsUpdate = true;
  }
}

export default CreatureEffects;
