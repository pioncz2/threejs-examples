import Example from './example';
import GameControls from './../utils/GameControls';

class Threejseditor extends Example {
  constructor() {
    super();

    this.camera.position.set(
      0,
      3.9428501285349356e-17,
      0.6439162918288116,
    );
    this.controls.dispose();
    this.controls = new GameControls(
      this.camera,
      this.renderer.domElement,
    );

    const loader = new THREE.ObjectLoader();

    loader.load(
      // resource URL
      'scenes/scene1.json',

      // onLoad callback
      // Here the loaded data is assumed to be an object
      (obj) => {
        // Add the loaded object to the scene
        this.scene.add(obj);
        const sceneChildren = this.scene.children[0].children;
        this.ui1 = sceneChildren.find((obj) => obj.name === 'ui1');
        this.ui2 = sceneChildren.find((obj) => obj.name === 'ui2');
        this.controls.on('keyup', (e) => {
          if (e.key === '1') {
            this.ui1.visible = false;
          }
          if (e.key === '2') {
            this.ui2.visible = false;
          }
        });
        this.controls.on('keydown', (e) => {
          if (e.key === '1') {
            this.ui1.visible = true;
          }
          if (e.key === '2') {
            this.ui2.visible = true;
          }
        });
      },

      // onProgress callback
      function (xhr) {
        console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
      },

      // onError callback
      function (err) {
        console.error('An error happened');
      },
    );
  }

  animate() {}
}

export default Threejseditor;
