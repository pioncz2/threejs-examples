import ParticleShader from './../shaders/particle';
import Example from './example';

const ParticleCount = 1800;

class Particles extends Example {
  constructor() {
    super();

    // create the particle variables
    this.particles = new THREE.BufferGeometry();
    const positions = [];
    const sizes = [];
    this.velocities = [];

    const pMaterial = new THREE.ShaderMaterial({
      uniforms: {
        time: { value: 1.0 },
        resolution: { value: new THREE.Vector2() },
        particleTexture: {
          value: this.loader.load('/images/particle2.png'),
        },
      },
      vertexShader: ParticleShader.vertexShader,
      fragmentShader: ParticleShader.fragmentShader,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true,
      vertexColors: true,
      side: THREE.DoubleSide,
    });

    this.particles.sortParticles = true;

    // now create the individual particles
    for (let p = 0; p < ParticleCount; p++) {
      const pX = Math.random() * 50 - 25;
      const pY = Math.random() * 50 - 25;
      const pZ = Math.random() * 50 - 100;

      positions.push(pX, pY, pZ);

      sizes.push(3.0);

      // create a velocity vector
      this.velocities.push(-Math.random() * 2);
    }

    this.particles.setAttribute(
      'position',
      new THREE.Float32BufferAttribute(positions, 3),
    );
    this.particles.setAttribute(
      'size',
      new THREE.Float32BufferAttribute(sizes, 1),
    );

    // create the particle system
    this.particleSystem = new THREE.Points(this.particles, pMaterial);

    // add it to the scene
    this.scene.add(this.particleSystem);
    this.objects = [this.particleSystem];
  }

  animate() {
    if (this.particleSystem) {
      // this.particleSystem.rotation.y += 0.001;

      let pCount = ParticleCount;
      while (pCount--) {
        const index = pCount * 3 + 1;
        const positionArray =
          this.particles.attributes.position.array;

        if (positionArray[index] < -25) {
          positionArray[index] = 50;
          this.velocities[pCount] = -Math.random();
        }

        positionArray[index] += this.velocities[pCount];
      }
      this.particles.attributes.position.needsUpdate = true;
    }
  }
}

export default Particles;
