import Stats from './../lib/Stats';
import OrbitControls from './../utils/OrbitControls';

class Example {
  constructor() {
    this.scene = new THREE.Scene();
    this.stats = new Stats();
    this.stats.showPanel(0);
    document.body.appendChild(this.stats.dom);
    this.scene.fog = new THREE.FogExp2(0x000000, 0.0025);
    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    );
    this.camera.position.z = 3;
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true,
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    document.body.appendChild(this.renderer.domElement);
    this.controls = new OrbitControls(
      this.camera,
      this.renderer.domElement,
    );
    this.objects = [];

    // Fps limit
    this.clock = new THREE.Clock();

    this.loader = new THREE.TextureLoader();

    window.addEventListener('resize', this.onWindowResize, false);

    setTimeout(() => {
      this.animateWrapper();
    }, 0);
  }

  onWindowResize = () => {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  };

  animateWrapper = () => {
    this.animationFrameId = window.requestAnimationFrame(
      this.animateWrapper,
    );

    this.stats.begin();

    let shouldRender = true;
    if (this.animate) {
      shouldRender = this.animate(this.clock.getDelta());
    }
    if (this.controls) {
      this.controls.update();
    }
    if (shouldRender !== false) {
      this.renderer.render(this.scene, this.camera);
    }
    this.stats.end();
  };

  remove() {
    if (this.animationFrameId) {
      window.cancelAnimationFrame(this.animationFrameId);
    }
    this.animationFrameId = null;
    for (let i = 0; i < this.objects.length; i++) {
      this.scene.remove(this.objects[i]);
      // lights doesnt have materials nor geometries
      if (this.objects[i].material) {
        this.objects[i].material.dispose();
      }
      if (this.objects[i].geometry) {
        this.objects[i].geometry.dispose();
      }
    }
    window.removeEventListener('resize', this.onWindowResize);
    document.body.removeChild(this.stats.dom);
    document.body.removeChild(this.renderer.domElement);
    this.renderer.dispose();
  }
}

export default Example;
