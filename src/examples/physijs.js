class PhysijsExample {
  constructor() {
    this.scene = new Physijs.Scene();

    this.scene.fog = new THREE.Fog(this.scene.background, 1, 5000);
    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    );
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.shadowMap.enabled = THREE.PCFSoftShadowMap; // to antialias the shadow
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement);

    const light = new THREE.HemisphereLight(0xffffbb, 0x080820, 0.1);
    this.scene.add(light);
    const directionalLight1 = new THREE.DirectionalLight(0xf8f7ea, 1);
    directionalLight1.castShadow = true;
    directionalLight1.shadow.mapSize.width = 1024;
    directionalLight1.shadow.mapSize.height = 1024;
    directionalLight1.position.set(-1, 2, -1);
    this.scene.add(directionalLight1);
    const directionalLight2 = new THREE.DirectionalLight(0xf8f7ea, 1);
    directionalLight2.castShadow = true;
    directionalLight2.shadow.mapSize.width = 1024;
    directionalLight2.shadow.mapSize.height = 1024;
    directionalLight2.position.set(-1, 2, 1);
    this.scene.add(directionalLight2);

    this.textures = [];
    this.objects = [];
    const loader = new THREE.TextureLoader();
    loader.load(
      'envMap3.png',
      (texture) => {
        this.textures.push(texture);
        for (let i = 0; i < 20; i++) {
          this.createRandomBox();
        }
      },
      // onProgress callback currently not supported
      undefined,
      // onError callback
      () => {
        throw new Error('An error happened.');
      },
    );

    this.cube2 = new Physijs.BoxMesh(
      new THREE.BoxGeometry(20, 0.01, 20),
      new THREE.MeshPhongMaterial({
        color: 0xffffff,
        flatShading: true,
        envMap: null,
      }),
      0,
    );
    this.cube2.receiveShadow = true;
    this.objects.push(this.cube2);
    this.scene.add(this.cube2);

    this.camera.position.y = 1;
    this.camera.position.z = 0;
    this.animating = true;
    this.animate();
    this.renderer.domElement.addEventListener(
      'click',
      this.createBall,
    );
    this.mousePosition = null;
    this.renderer.domElement.addEventListener(
      'mousemove',
      (event) => {
        this.mousePosition = new THREE.Vector3(
          (event.pageX / window.innerWidth) * 2 - 1,
          -(event.pageY / window.innerHeight) * 2 + 1,
          0.5,
        );
      },
    );
  }

  createRandomBox = () => {
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshPhongMaterial({
      map: this.textures[0],
      flatShading: true,
      envMap: null,
    });
    const cube = new Physijs.BoxMesh(geometry, material);

    cube.position.x = Math.random() * 4 + 4;
    cube.position.y = 5;
    cube.position.z = -(Math.random() * 4 + 4);
    cube.castShadow = true;
    this.objects.push(cube);
    this.scene.add(cube);
  };

  createBall = () => {
    const geometry = new THREE.SphereGeometry(0.2, 32, 32);
    const material = Physijs.createMaterial(
      new THREE.MeshPhongMaterial({
        map: this.textures[0],
        flatShading: true,
        envMap: null,
      }),
      0.8, // friction
      0.3, // restitution (bounciness)
    );
    const ball = new Physijs.SphereMesh(geometry, material, 4);

    ball.position.set(
      this.camera.position.x,
      this.camera.position.y,
      this.camera.position.z,
    );
    ball.castShadow = true;
    this.objects.push(ball);
    this.scene.add(ball);

    const dir = this.mousePosition
      .unproject(this.camera)
      .sub(this.camera.position)
      .normalize();
    ball.setLinearVelocity(dir.clone().multiplyScalar(30));
  };

  animate() {
    if (!this.animating) return;
    this.scene.simulate();
    this.renderer.render(this.scene, this.camera);
    this.animationFrameId = window.requestAnimationFrame(
      this.animate,
    );
  }

  remove() {
    if (this.animationFrameId) {
      window.cancelAnimationFrame(this.animationFrameId);
    }
    this.animationFrameId = null;
    for (let i = 0; i < this.objects.length; i++) {
      const obj = this.objects[i];

      this.scene.remove(obj);
      obj.remove();
      if (obj.material) {
        obj.material.dispose();
      }
      if (obj.geometry) {
        obj.geometry.dispose();
      }
    }
    document.body.removeChild(this.renderer.domElement);
    this.renderer.dispose();
  }
}

export default PhysijsExample;
