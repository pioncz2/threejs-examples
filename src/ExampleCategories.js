const ExampleCategories = {
  experiments: 'Experiments',
  shaders: 'Shaders',
  gameMechanichs: 'Game Mechanics',
};

export default ExampleCategories;
