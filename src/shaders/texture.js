const TextureShader = {
  uniforms: {
    tDiffuse: { value: null },
  },

  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
    `,

  fragmentShader: `
    uniform sampler2D tDiffuse;
    uniform sampler2D particleTexture;
    varying vec2 vUv;
    void main(){
      vec4 base = texture2D( particleTexture, vUv ); // texture pixel
      gl_FragColor = base * vec4( vec3( vUv, 0. ), 1. ); // base * rgba based on uv
    }
    `,
};

export default TextureShader;
