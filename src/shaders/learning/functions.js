// vUv - pixel position in the texture

// mix(valueA, valueB, pct) - mix values A and B in percentages
// step(treshold, value): return 0.0 if value is below treshold, 1.0 otherwise
// smoothstep(edge0, edge1, value): interpolate value between edge0 and edge1.
// y = mod(x,0.5); // return x modulo of 0.5
// y = fract(x); // return only the fraction part of a number
// y = ceil(x);  // nearest integer that is greater than or equal to x
// y = floor(x); // nearest integer less than or equal to x
// y = sign(x);  // extract the sign of x
// y = abs(x);   // return the absolute value of x
// y = clamp(x,0.0,1.0); // constrain x to lie between 0.0 and 1.0
// y = min(0.0,x);   // return the lesser of x and 0.0
// y = max(0.0,x);   // return the greater of x and 0.0

// length(x): x specifies a vector of which to calculate the length.
// distance(x, y): distance between vectors
// dot(), cross, normalize(), faceforward(), reflect() and refract().
// Also GLSL has special vector relational functions such as:
// lessThan(), lessThanEqual(), greaterThan(), greaterThanEqual(), equal() and notEqual().

const Function1 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    void main() {
      // Do it when shaders is fullscreen, otherwise use vUv
      // vec2 st = gl_FragCoord.xy/u_resolution;
      vec2 st = vUv.xy;

      gl_FragColor=vec4(
        st.x,
        st.y,
        abs(sin(u_time)) / 2.0 + (u_mouse.x / u_resolution.x) / 2.0,
        1.0
      );
    }
  `,
};

const Function2 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    // Plot a line on Y using a value between 0.0-1.0
    float plot(vec2 st, float pct){
      return smoothstep( pct-0.02, pct, st.y) - smoothstep( pct, pct+0.02, st.y);
    }
    
    void main() {
      float y = smoothstep(0.1,0.9,vUv.x);
    
      vec3 color = vec3(y);
    
      // Plot a line
      float pct = plot(vUv,y);
      color = (1.0-pct)*color+pct*vec3(0.0,1.0,0.0);
    
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function3 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    // Plot a line on Y using a value between 0.0-1.0
    float plot(vec2 st, float pct){
      return smoothstep( pct-0.02, pct, st.y) - smoothstep( pct, pct+0.02, st.y);
    }
    
    void main() {
      float y = smoothstep(0.2,0.5,vUv.x) - smoothstep(0.5,0.8,vUv.x);
    
      vec3 color = vec3(y);
    
      // Plot a line
      float pct = plot(vUv,y);
      color = (1.0-pct)*color+pct*vec3(0.0,1.0,0.0);
    
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function4 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    vec3 colorA = vec3(0.149,0.141,0.912);
    vec3 colorB = vec3(1.000,0.833,0.224);
    
    
    void main() {
      vec3 color = vec3(0.0);

      float pct = abs(sin(u_time));
  
      // Mix uses pct (a value from 0-1) to
      // mix the two colors
      color = mix(colorA, colorB, pct);
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function5 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #define PI 3.14159265359

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    vec3 colorA = vec3(0.237,0.886,0.912);
    vec3 colorB = vec3(0.895,1.000,0.281);

    float plot (vec2 st, float pct){
      return  smoothstep( pct-0.01, pct, st.y) -
              smoothstep( pct, pct+0.01, st.y);
    }
    
    void main() {
      vec3 color = vec3(0.0);
  
      vec3 pct = vec3(vUv.x);
  
      pct.r = smoothstep(0.0,1.0, vUv.x);
      pct.g = sin(vUv.x*PI);
      pct.b = pow(vUv.x,0.5);
  
      color = mix(colorA, colorB, pct);
  
      // Plot transition lines for each channel
      color = mix(color,vec3(1.0,0.0,0.0),plot(vUv,pct.r));
      color = mix(color,vec3(0.0,1.0,0.0),plot(vUv,pct.g));
      color = mix(color,vec3(0.0,0.0,1.0),plot(vUv,pct.b));
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function6 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #define PI 3.14159265359

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    vec3 colorA = vec3(0.875,0.940,1.000);
    vec3 colorB = vec3(0.237,0.886,0.912);

    float plot (vec2 st, float pct){
      return  smoothstep( pct-0.15, pct, st.y) -
              smoothstep( pct, pct+0.15, st.y);
    }
    
    void main() {
      vec3 color = mix(colorA, colorB, vUv.y);

      vec4 pct = vec4(vUv.x);

      pct.r = sin(vUv.x*PI*0.25 + 0.375 * PI) - 0.2;
      pct.g = sin(vUv.x*PI*0.25 + 0.375 * PI) - 0.3;
      pct.b = sin(vUv.x*PI*0.25 + 0.375 * PI) - 0.4;
      pct.a = sin(vUv.x*PI*0.25 + 0.375 * PI) - 0.5;

      // Plot transition lines for each channel
      color = mix(color,vec3(1.0,0.0,0.0),plot(vUv,pct.r));
      color = mix(color,vec3(1.0,1.0,0.0),plot(vUv,pct.g));
      color = mix(color,vec3(0.0,1.0,0.0),plot(vUv,pct.b));
      color = mix(color,vec3(0.0,0.0,1.0),plot(vUv,pct.a));

      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function7 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #define TWO_PI 6.28318530718

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    //  Function from Iñigo Quiles
    //  https://www.shadertoy.com/view/MsS3Wc
    vec3 hsb2rgb( in vec3 c ){
        vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),
                                 6.0)-3.0)-1.0,
                         0.0,
                         1.0 );
        rgb = rgb*rgb*(3.0-2.0*rgb);
        return c.z * mix( vec3(1.0), rgb, c.y);
    }
    
    void main() {
      vec2 st = vUv;
      vec3 color = vec3(0.0);
  
      // Use polar coordinates instead of cartesian
      vec2 toCenter = vec2(0.5)-st;
      float angle = atan(toCenter.y,toCenter.x) + u_time;
      float radius = length(toCenter)*2.0;
  
      // Map the angle (-PI to PI) to the Hue (from 0 to 1)
      // and the Saturation to the radius
      color = hsb2rgb(vec3((angle/TWO_PI)+0.5,radius,1.0));
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function8 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;

    void main(){
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      vec2 bl = smoothstep(0.2, 0.3, st);  // bottom-left blurred
      vec2 tr = step(vec2(0.2,0.3),1.0-st);   // top-right

      color = vec3(bl.x * bl.y * tr.x * tr.y);
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function9 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
  varying vec2 vUv;

  uniform vec2 u_resolution;
  uniform float u_time;

  vec3 rect( in vec2 st, in vec2 bl, in vec2 tr, in bool blur, in float blurWidth){
    vec2 blStep = vec2(0);
    vec2 trStep = vec2(0);

    if (blur) {
      blStep = vec2(smoothstep(bl.x, bl.x + blurWidth, st.x), smoothstep(bl.y, bl.y + blurWidth, st.y));  // bottom-left blurred
      trStep = 1.0 - vec2(smoothstep(tr.x - blurWidth, tr.x, st.x), smoothstep(tr.y - blurWidth, tr.y, st.y));   // top-right blurre
    } else {
      blStep = step(bl,st);   // top-right
      trStep = step(1.0-tr,1.0-st);   // top-right
    }

    return vec3(blStep.x * blStep.y * trStep.x * trStep.y);
  }

  vec3 strokeRect( in vec2 st, in vec2 bl, in vec2 tr, in float width){
    return vec3(
      ( // bigger square
        step(bl.x - width, st.x) * // greater than bl.x - width
        step(1.0 - tr.x - width, 1.0 - st.x) * // smaller than tr.x + width
        step(bl.y - width, st.y) *  // greater than bl.y - width
        step(1.0 - tr.y - width, 1.0 - st.y) // smaller than tr.y + width
      ) - ( // substract smaller square
        step(bl.x, st.x) *
        step(1.0 - tr.x, 1.0 - st.x) *
        step(bl.y, st.y) *
        step(1.0 - tr.y, 1.0 - st.y)
      )
  );

  }

  void main(){
    vec2 st = vUv.xy;
    vec3 color = vec3(0.0);

    color = rect(
      st, 
      vec2(abs(mod(u_time,1.0) - 0.5), 0.4), 
      vec2(1.0 - abs(mod(u_time,1.0) - 0.5), 0.6),
      true, // blur
      0.3 * abs(mod(u_time, 1.0) - 0.5) // blur width
    ) + 0.4 * strokeRect(
      st, 
      vec2(abs(mod(u_time,1.0) - 0.5), 0.4), 
      vec2(1.0 - abs(mod(u_time,1.0) - 0.5), 0.6),
      // vec2(0.1, 0.1), 
      // vec2(0.9, 0.9),
      0.1
    );

    gl_FragColor = vec4(color,1.0);
  }
  `,
};

const Function10 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
  varying vec2 vUv;

  uniform vec2 u_resolution;
  uniform float u_time;

  void main(){
    vec2 st = vUv.xy;
    float pct = 0.0;

    // a. The DISTANCE from the pixel to the center
    // pct = distance(st,vec2(0.5));

    // b. The LENGTH of the vector
    //    from the pixel to the center
    // vec2 toCenter = vec2(0.5)-st;
    // pct = length(toCenter);

    // c. The SQUARE ROOT of the vector
    //    from the pixel to the center
    // vec2 tC = vec2(0.5)-st;
    // pct = sqrt(tC.x*tC.x+tC.y*tC.y);

    // pct *= 2.0;

    // combine 2 distances for various effects
    //pct = distance(st,vec2(0.4)) + distance(st,vec2(0.6));
    //pct = distance(st,vec2(0.4)) * distance(st,vec2(0.6));
    //pct = min(distance(st,vec2(0.4)),distance(st,vec2(0.6)));
    //pct = max(distance(st,vec2(0.4)),distance(st,vec2(0.6)));
    pct = pow(distance(st,vec2(0.4)),distance(st,vec2(0.6)));

    vec3 color = vec3(pct);

    gl_FragColor = vec4( color, 1.0 );
  }
  `,
};

const Function11 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;

    float circle( in vec2 st, in vec2 pos, in float radius, in float blurWidth ) {
      vec2 toCenter = pos-st;
      float len = 2.0 * length(toCenter);
      return 1.0 - smoothstep(radius-blurWidth, radius, len);
    }

    void main(){
      vec2 st = vUv.xy;
      vec3 color = vec3(0);
      vec2 pos1 = vec2(
        sin(u_time*1.3)/8.0 + 0.5,
        cos(u_time*1.3)/8.0 + 0.5
      );
      vec2 pos2 = vec2(
        sin(u_time*1.3 + 2.09)/8.0 + 0.5,
        cos(u_time*1.3 + 2.09)/8.0 + 0.5
      );
      vec2 pos3 = vec2(
        sin(u_time*1.3 + 4.19)/8.0 + 0.5,
        cos(u_time*1.3 + 4.19)/8.0 + 0.5
      );

      //circle 1
      color += vec3(0.0, 1.0, 1.0) * circle(
        st, // pixel xy
        pos1, // circle center
        0.35, // radius
        0.1 // blurWidth
      );
      //circle 2
      color += vec3(1.0, 0.0, 1.0) * circle(
        st, // pixel xy
        pos2, // circle center
        0.35, // radius
        0.1 // blurWidth
      );
      //circle 3
      color += vec3(1.0, 1.0, 0.0) * circle(
        st, // pixel xy
        pos3, // circle center
        0.35, // radius
        0.1 // blurWidth
      );

      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function12 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
  varying vec2 vUv;

  uniform vec2 u_resolution;
  uniform float u_time;

  void main(){
    vec2 st = vUv.xy;
    vec3 color = vec3(1.0, 1.0, 0.0);
    // st.x *= u_resolution.x/u_resolution.y;
    float d = 0.0;
  
    // Remap the space to -1. to 1.
    st = st *2.-1.;
  
    // Make the distance field
    d = length( abs(st)-.3 );
    // d = length( min(abs(st)-.3,0.) );
    // d = length( max(abs(st)-.3,0.) );
  
    // Visualize the distance field
    gl_FragColor = vec4(color * vec3(fract(d*10.0)),1.0);
  
    // Drawing with the distance field
    // gl_FragColor = vec4(vec3( step(.3,d) ),1.0);
    // gl_FragColor = vec4(vec3( step(.3,d) * step(d,.4)),1.0);
    // gl_FragColor = vec4(vec3( smoothstep(.3,.4,d)* smoothstep(.6,.5,d)) ,1.0);

//    gl_FragColor = vec4(color,1.0);
  }
  `,
};

const Function13 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    void main() {
      // vec2 st = gl_FragCoord.xy/u_resolution;
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
  
      vec2 pos = vec2(0.5)-st;
  
      float r = length(pos)*2.0;
      float a = atan(pos.y,pos.x);
  
      // shaping function
      float f = sin(a*2.);
      // f = abs(cos(a*3.));
      // f = abs(cos(a*2.5))*.5+.3;
      // f = abs(cos(a*12.)*sin(a*3.))*.8+.1;
      // f = smoothstep(-0.9,1., cos(a*10.))*0.2+0.5;
  
      color = vec3( 1.-smoothstep(f,f+0.02,r) );
  
      gl_FragColor = vec4(color, 1.0);
    }
  `,
};

const Function14 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float plot (vec2 st, float pct, float width){
      return  smoothstep( pct-width, pct, st.y) -
              smoothstep( pct, pct+width, st.y);
    }

    void main() {
      // vec2 st = gl_FragCoord.xy/u_resolution;
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
  
      vec2 pos = vec2(0.5)-st;
  
      float r = length(pos)*2.0;
      float a = atan(pos.y,pos.x) + u_time;
      float f = (0.5 + smoothstep(-0.9,1., cos(a*10.))*0.2);

      color = vec3(0.5) * (1.0 - step(f, r)); // draw color with function shape
      color += vec3(0.3) * (step(0.2,r) - step(0.4,r)); // draw ring
      color -= vec3(1.0) * (1.0 - step(0.25, r)); // substract circle

      gl_FragColor = vec4(color, 1.0);
    }
  `,
};

const Function15 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265359
    #define TWO_PI 6.28318530718

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    vec3 polygon( vec2 pos, float sides ) {
      // Angle and radius from the current pixel
      float a = atan(pos.x,pos.y)+PI;
      float r = TWO_PI/float(sides);
    
      // Shaping function that modulate the distance
      float d = cos(floor(.5+a/r)*r-a)*length(pos);
    
      return vec3(1.0-smoothstep(.4,.41,d));
    }

    void main() {
      // vec2 st = gl_FragCoord.xy/u_resolution;
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
    
      // Remap the space to -1. to 1.
      st = st *2.-1.;
      color = polygon(st, 5.0);
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function16 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265359

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float box(in vec2 _st, in vec2 _size){
      _size = vec2(0.5) - _size*0.5;
      vec2 uv = smoothstep(
        _size,
        _size+vec2(0.001),
        _st);
      uv *= smoothstep(
        _size,
        _size+vec2(0.001),
        vec2(1.0)-_st
      );
      return uv.x*uv.y;
    }
    
    float cross(in vec2 _st, float _size){
      return  box(_st, vec2(_size,_size/4.)) +
              box(_st, vec2(_size/4.,_size));
    }

    mat2 rotate2d(float _angle){
      return mat2(cos(_angle),-sin(_angle),
                  sin(_angle),cos(_angle));
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      // To move the cross we move the space
      vec2 translate = 0.7 * vec2(
        sin(u_time),
        cos(u_time*2.0 + PI/2.0 ) * 0.5
      );
      translate = rotate2d(0.3) * translate;
      st += translate*0.5;
  
      // Show the coordinates of the space on the background
      color = vec3(st.x,st.y,0.0);
  
      // Add the shape on the foreground
      color += vec3(cross(st,0.25));
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function17 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.1, 1.0, 0.9);
      // st.x *= u_resolution.x/u_resolution.y;
      float d = 0.0;
    
      // Remap the space to -1. to 1.
      st = st *2.-1.;
    
      // Make the distance field
      d = length( abs(st)-.3 );
      // d = length( min(abs(st)-.3,0.) );
      // d = length( max(abs(st)-.3,0.) );
    
      // Visualize the distance field
      gl_FragColor = vec4(color * vec3(fract(d*(80.0 + sin(u_time) * 20.0))),1.0);
      // gl_FragColor = vec4(color * vec3(fract(d*80.0)),1.0);
    
      // Drawing with the distance field
      // gl_FragColor = vec4(vec3( step(.3,d) ),1.0);
      // gl_FragColor = vec4(vec3( step(.3,d) * step(d,.4)),1.0);
      // gl_FragColor = vec4(vec3( smoothstep(.3,.4,d)* smoothstep(.6,.5,d)) ,1.0);
  
  //    gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function18 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265359

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    mat2 rotate2d(float _angle){
      return mat2(cos(_angle),-sin(_angle),
                  sin(_angle),cos(_angle));
    }
    
    mat2 scale(vec2 _scale){
      return mat2(_scale.x,0.0,
                  0.0,_scale.y);
    }

    float box(in vec2 _st, in vec2 _size){
        _size = vec2(0.5) - _size*0.5;
        vec2 uv = smoothstep(_size,
                            _size+vec2(0.001),
                            _st);
        uv *= smoothstep(_size,
                        _size+vec2(0.001),
                        vec2(1.0)-_st);
        return uv.x*uv.y;
    }
    
    float cross(in vec2 _st, float _size){
        return  box(_st, vec2(_size,_size/4.)) +
                box(_st, vec2(_size/4.,_size));
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      // move space from the center to the vec2(0.0)
      st -= vec2(0.5);
      st = rotate2d( sin(u_time)*PI ) * st;
      st = scale( vec2(sin(u_time)+1.0) ) * st;
      // move it back to the original place
      st += vec2(0.5);

      // Show the coordinates of the space on the background
      color = vec3(st.x,st.y,0.0);
  
      // Add the shape on the foreground
      color += vec3(cross(st,0.4));
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function19 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    // YUV to RGB matrix
    mat3 yuv2rgb = mat3(1.0, 0.0, 1.13983,
                        1.0, -0.39465, -0.58060,
                        1.0, 2.03211, 0.0);
    
    // RGB to YUV matrix
    mat3 rgb2yuv = mat3(0.2126, 0.7152, 0.0722,
                        -0.09991, -0.33609, 0.43600,
                        0.615, -0.5586, -0.05639);

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      // UV values goes from -1 to 1
      // So we need to remap st (0.0 to 1.0)
      st -= 0.5;  // becomes -0.5 to 0.5
      st *= 2.0;  // becomes -1.0 to 1.0
  
      // we pass st as the y & z values of
      // a three dimensional vector to be
      // properly multiply by a 3x3 matrix
      color = yuv2rgb * vec3(0.5, st.x, st.y);
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function20 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float circle(in vec2 _st, in float _radius){
      vec2 l = _st-vec2(0.5);
      return 1.-smoothstep(_radius-(_radius*0.01),
                           _radius+(_radius*0.01),
                           dot(l,l)*4.0);
    }
    
    vec2 tile(vec2 st, float rows, float columns) {
        st *= vec2(rows, columns); // Divide space
        return fract(st); // Wrap arround 1.0
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
      float rows = 2.0;
      float columns = 4.0;
  
      // Tile positions
      float x = floor(st.x*rows);
      float y = floor(st.y*columns);
      
      st = tile(st, rows, columns);
      
      // Now we have 3 spaces that goes from 0-1
      color = vec3(st,0.0);
      color += vec3(circle(st,0.05+(0.1*(x+y))));
    
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function21 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    #define PI 3.14159265358979323846

    vec2 rotate2D(vec2 _st, float _angle){
      _st -= 0.5;
      _st =  mat2(cos(_angle),-sin(_angle),
                  sin(_angle),cos(_angle)) * _st;
      _st += 0.5;
      return _st;
    }

    vec2 tile(vec2 _st, float _zoom){
      _st *= _zoom;
      return fract(_st);
    }

    float box(vec2 _st, vec2 _size, float _smoothEdges){
      _size = vec2(0.5)-_size*0.5;
      vec2 aa = vec2(_smoothEdges*0.5);
      vec2 uv = smoothstep(_size,_size+aa,_st);
      uv *= smoothstep(_size,_size+aa,vec2(1.0)-_st);
      return uv.x*uv.y;
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      // Divide the space in 4
      st = tile(st,4.);
  
      color = vec3(st,0.0);
      
      // Use a matrix to rotate the space 45 degrees
      st = rotate2D(st,PI*0.25+tan(u_time));
  
      // Draw squares
      color += vec3(box(st,vec2(0.7),0.1)) 
          * vec3(1.0 - st.x,0.2+0.8*st.x,1.0*st.x);
      color += vec3(box(st,vec2(0.5),0.01)) 
          * vec3(1.0 - st.x,0.2+0.8*st.x,1.0*st.x);
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function22 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    vec2 brickTile(vec2 _st, float _zoom, float xOffset, float yOffset){
      _st *= _zoom;
  
      // Here is where the offset is happening
      _st.x += step(1., mod(_st.y,2.0)) * xOffset;
      _st.y += step(1., mod(_st.x,2.0)) * yOffset; //

      return fract(_st);
    }
    
    float box(vec2 _st, vec2 _size){
      _size = vec2(0.5)-_size*0.5;
      vec2 uv = smoothstep(_size,_size+vec2(1e-4),_st);
      uv *= smoothstep(_size,_size+vec2(1e-4),vec2(1.0)-_st);
      return uv.x*uv.y;
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);

      // Modern metric brick of 215mm x 102.5mm x 65mm
      // http://www.jaharrison.me.uk/Brickwork/Sizes.html
      st /= vec2(2.15,0.65)/1.5;
      
      float xOffset = step(0.5, mod(u_time, 1.0)) * fract(u_time) * 2.0;
      float yOffset = (1.0 - step(0.5, mod(u_time, 1.0))) * fract(u_time) * 2.0;

      // Apply the brick tiling
      st = brickTile(
        st,
        5.0,
        xOffset,
        yOffset
      );
  
      color = vec3(box(st,vec2(0.9)));
  
      // Uncomment to see the space coordinates
      // color = vec3(st,0.0);
  
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function23 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    vec2 rotate2D (vec2 _st, float _angle) {
      _st -= 0.5;
      _st =  mat2(cos(_angle),-sin(_angle),
                  sin(_angle),cos(_angle)) * _st;
      _st += 0.5;
      return _st;
  }
  
  vec2 tile (vec2 _st, float _zoom) {
      _st *= _zoom;
      return fract(_st);
  }
  
  vec2 rotateTilePattern(vec2 _st){
  
      //  Scale the coordinate system by 2x2
      _st *= 2.0;
  
      //  Give each cell an index number
      //  according to its position
      float index = 0.0;
      index += step(1., mod(_st.x,2.0));
      index += step(1., mod(_st.y,2.0))*2.0;
  
      //      |
      //  2   |   3
      //      |
      //--------------
      //      |
      //  0   |   1
      //      |
  
      // Make each cell between 0.0 - 1.0
      _st = fract(_st);
  
      // Rotate each cell according to the index
      if(index == 1.0){
          //  Rotate cell 1 by 90 degrees
          _st = rotate2D(_st,PI*0.5);
      } else if(index == 2.0){
          //  Rotate cell 2 by -90 degrees
          _st = rotate2D(_st,PI*-0.5);
      } else if(index == 3.0){
          //  Rotate cell 3 by 180 degrees
          _st = rotate2D(_st,PI);
      }
  
      return _st;
  }

    void main() {
      vec2 st = vUv.xy;

      st = tile(st,3.0);
      st = rotateTilePattern(st);
  
      // Make more interesting combinations
      // st = tile(st,2.0);
      // st = rotate2D(st,-PI*u_time*0.25);
      st = rotateTilePattern(st*2.);
      st = rotate2D(st,PI*u_time*0.25);
  
      // step(st.x,st.y) just makes a b&w triangles
      // but you can use whatever design you want.
      gl_FragColor = vec4(vec3(step(st.x,st.y)),1.0);
    }
  `,
};

const Function24 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265359
    #define TWO_PI 6.28318530718

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    vec3 polygon( vec2 pos, float sides ) {
      // Angle and radius from the current pixel
      float a = atan(pos.x,pos.y)+PI;
      float r = TWO_PI/float(sides);
    
      // Shaping function that modulate the distance
      float d = cos(floor(.5+a/r)*r-a)*length(pos);
    
      return vec3(1.0-smoothstep(.95,.951,d));
    }

    void main() {
      // vec2 st = gl_FragCoord.xy/u_resolution;
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
      
      st *= 3.0;      // Scale up the space by 3
      st.x -= -.5*(floor(st.x/3.0)+1.0);
      st.y += step(1., mod(st.x,2.0)) * 0.5;

      st = fract(st); // Wrap arround 1.0

      // Remap the space to -1. to 1.
      st = st *2.-1.;
      st.x *= 1.15;
      color = polygon(st, 6.0);
      gl_FragColor = vec4(color,1.0);
    }
  `,
};

const Function25 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    // Pseudo random number generator
    float random (vec2 st) {
      return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
    }
    
    void main() {
      vec2 st = vUv.xy;
      float rnd = random( st );

      gl_FragColor = vec4(vec3(rnd),1.0);
    }
  `,
};

const Function26 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform vec2 u_mouse;
    uniform float u_time;

    // Pseudo random number generator
    float random (vec2 st) {
      return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
    }
    
    void main() {
      vec2 st = vUv.xy;

      st = st *2.-1.; // Center the coordinate system
      st *= 8.0 + sin(u_time) * 5.0; // Scale the coordinate system

      float rnd = random( floor(st) );



      gl_FragColor = vec4(vec3(rnd),1.0);
    }
  `,
};

const Function27 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float random (in vec2 _st) {
      return fract(sin(dot(_st.xy,
                           vec2(12.9898,78.233)))*
          43758.5453123);
    }
    
    vec2 truchetPattern(in vec2 _st, in float _index){
        _index = fract(((_index-0.5)*2.0));
        if (_index > 0.75) {
            _st = vec2(1.0) - _st;
        } else if (_index > 0.5) {
            _st = vec2(1.0-_st.x,_st.y);
        } else if (_index > 0.25) {
            _st = 1.0-vec2(1.0-_st.x,_st.y);
        }
        return _st;
    }

    void main() {
      vec2 st = vUv.xy;
      st *= 10.0;
      st = (st-vec2(5.0))*(abs(sin(u_time*0.2))*5.);
      st.x += u_time*3.0;
  
      vec2 ipos = floor(st);  // integer
      vec2 fpos = fract(st);  // fraction
  
      vec2 tile = truchetPattern(fpos, random( ipos ));
  
      float color = 0.0;
  
      // Maze
      color = smoothstep(tile.x-0.3,tile.x,tile.y)-
              smoothstep(tile.x,tile.x+0.3,tile.y);
  
      // Circles
      color = (step(length(tile),0.6) -
               step(length(tile),0.4) ) +
              (step(length(tile-vec2(1.)),0.6) -
               step(length(tile-vec2(1.)),0.4) );
  
      // Truchet (2 triangles)
      // color = step(tile.x,tile.y);
  
      gl_FragColor = vec4(vec3(color),1.0);
    }
  `,
};

const Function28 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float random (in vec2 _st) {
      return fract(sin(dot(_st.xy,
                           vec2(12.9898,78.233)))*
          43758.5453123);
    }

    float random (in float x, in float p) {
      return fract(sin(x * 12.9898 + (p*2.22))*43758.5453123);
    }

    void main() {
      vec2 st = vUv.xy;
      float scale = 50.0;
      float rows = 12.0;
      float rowHeight = scale / rows;
      float timeP = mod(floor(u_time / 5.0), 10.0);
      float velocity = 1.0 + random(timeP, 2.0) * 5.0;

      st *= scale;

      float iposy = ceil(
        st.y / rowHeight
      );

      float iposx = floor(
        st.x - (
          u_time * velocity * (random(iposy, timeP) + 1.0) * iposy
          * (random(iposy, timeP) * 2.0 - 1.0) // random direction
        )
      );

      float color = random(iposx, timeP) * 1.5;

      // lines between rows
      color += 1.0 - step(mod(st.y, rowHeight)/rowHeight, 0.95);
      color += step(mod(st.y, rowHeight)/rowHeight, 0.05);

      gl_FragColor=vec4(
        vec3(color),
        1.0
      );
    }
  `,
};

const Function29 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846
    #define PI2 1.5707963267948966

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float random (in float x) {
      return fract(sin(x * 12.9898)*43758.5453123);
    }

    float noise (in float x, in float t) {
      float i = floor(x) + floor(t);  // integer
      float f = fract(x);  // fraction
      // return random(i);
      return mix(random(i), random(i + 1.0), fract(t));
      // return mix(random(i), random(i + 1.0), smoothstep(0.,1.,f));
    }

    // accepts 0.0 - 1.0
    float ease1 (in float x) {
      float x2 = mod(x, 1.0);

      if (x2 < 0.25) {
        x2 = (x2 * PI2)/0.25;
      } else {
        x2 = PI2 - (x2-0.25)*PI2 / 0.75;
      }
      return sin(x2);
    }

    vec3 hsb2rgb( in vec3 c ){
      vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),
                               6.0)-3.0)-1.0,
                       0.0,
                       1.0 );
      rgb = rgb*rgb*(3.0-2.0*rgb);
      return c.z * mix( vec3(1.0), rgb, c.y);
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
      
      vec2 pos = vec2(0.5)-st;
      
      float bars = 100.0;
      float circlePart = 2.0 * PI / bars;
      float t = sin(u_time);
      float innerR = 0.2;
      float r = length(pos)*2.0;
      float a = atan(pos.y,pos.x);

      float f = 0.0; 

      f = innerR; // inner circle
      f += noise(mod(a+t, 6.28)/circlePart, u_time) * 0.4; // shaping function
      f += ease1(fract(u_time)) * 0.4; // dynamic grow

      color = vec3(1.0 - step(innerR + f, r));

      // colorize
      color *= hsb2rgb(vec3( abs(a)/PI/4.8, 1.0, 1.0 ));

      // make the spaces between bars
      color -= .3 * smoothstep(0.75 * circlePart, circlePart, mod(a, circlePart));

      gl_FragColor = vec4(color, 1.0);
    }
  `,
};

const Function30 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float random (in float x) {
      return fract(sin(x * 12.9898)*43758.5453123);
    }

    float noise (in float x, in float t) {
      float i = floor(x) + floor(t);  // integer
      float f = fract(x);  // fraction
      // return random(i);
      return mix(random(i), random(i + 1.0), fract(t));
      // return mix(random(i), random(i + 1.0), smoothstep(0.,1.,f));
    }

    void main() {
      vec2 st = vUv.xy;
      vec3 color = vec3(0.0);
      
      vec2 pos = vec2(0.5)-st;
      
      // bialo czarny
      // kontury bialy
      // w srodku plamy
      // na calosc nalozony noise: albo rozny przy krawedziach i w srodku, albo taki sam

      float parts = 6.0;
      float circlePart = 2.0 * PI / parts;
      float t = sin(u_time);
      float innerR = 0.2;
      float r = length(pos)*2.0;
      float a = atan(pos.y,pos.x);

      float f = 0.0; 

      if (r > 0.3) {
        f = 0.7 + sin((a+t/3.0) * parts) * 0.06; // shaping function
        color = vec3(1.0 - step(f, r));
        // darken closer values
        color -= vec3(1.0 - smoothstep(0.5, f, r));
        // make gradient
        color -= 0.3 * smoothstep(0.75 * circlePart, circlePart, mod((a+u_time/2.0), circlePart));
        // make the spaces between bars
  
        // color -= 1.0 * smoothstep(0.95 * circlePart, 1. * circlePart, mod((a +t/3.0), circlePart));
        color -= step(0.90 * circlePart, mod((a+t/3.0 + PI + 0.25), circlePart));
        color -= 1.0 - step(0.10 * circlePart, mod((a +t/3.0 + PI + 0.25), circlePart));
        
        // dark noise
        color -= 0.2 * random(st.x * st.y + t);
      } else {
        f = 0.2 + sin((a+u_time*3.0) * 2.0) * 0.01; // shaping function

        // draw circle
        color += 1.0 - step(f, r);
        
        // outside particles
        float h = (smoothstep(f+0.08, f, r) - (1.0 - step(f,r))) * random(st.x + st.y + t);
        color += vec3(0.0, h * 0.7, h);

        // rotating border
        color += step(f+0.1, r);
        
        // inner noise
        color -= 0.2 * random(st.x * st.y + t) * r/.2 * (1.0 - step(f, r));

        // inner color
        color -= (1.0 - step(f, r)) * vec3(.4, .0, .4);
      }

      gl_FragColor = vec4(color, 1.0);
    }
  `,
};

const Function31 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    #define PI 3.14159265358979323846

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    float line (in vec2 startPos, in vec2 endPos, in vec2 st) {
      float a = (endPos.y - startPos.y) / (endPos.x - startPos.x);
      float b = startPos.y - a * startPos.x;
      float y = a * st.x + b;
      float thickness = 0.005;

      return (step(y, st.y) - step(y+thickness, st.y)) * (step(startPos.x, st.x) - step(endPos.x, st.x));
    }

    void main() {
      vec2 st = vUv.xy - vec2(0.5);
      vec3 color = vec3(0.0, 0.0, 0.0);
      float r = 0.4;
      float n = 6.0;
      float angle = 40.0 * PI / 180.0;

      vec2 start = vec2(-0.1+sin(u_time)/10.0, 0.1-sin(u_time)/10.0);
      vec2 end = vec2(
        r * sin(angle),
        r * cos(angle)
      );

      color += line(start, end, st);

      gl_FragColor=vec4(color, 1.0);
    }
  `,
};

const Function32 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    void main() {
      vec2 st = vUv.xy;

      gl_FragColor=vec4(
        st.x,
        st.y,
        abs(sin(u_time)) / 2.0 + (u_mouse.x / u_resolution.x) / 2.0,
        1.0
      );
    }
  `,
};

const Function33 = {
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }
  `,
  fragmentShader: `
    #ifdef GL_ES
    precision mediump float;
    #endif

    varying vec2 vUv;

    uniform vec2 u_resolution;
    uniform float u_time;
    uniform vec2 u_mouse;

    void main() {
      vec2 st = vUv.xy;

      gl_FragColor=vec4(
        st.x,
        st.y,
        abs(sin(u_time)) / 2.0 + (u_mouse.x / u_resolution.x) / 2.0,
        1.0
      );
    }
  `,
};

export {
  Function1,
  Function2,
  Function3,
  Function4,
  Function5,
  Function6,
  Function7,
  Function8,
  Function9,
  Function10,
  Function11,
  Function12,
  Function13,
  Function14,
  Function15,
  Function16,
  Function17,
  Function18,
  Function19,
  Function20,
  Function21,
  Function22,
  Function23,
  Function24,
  Function25,
  Function26,
  Function27,
  Function28,
  Function29,
  Function30,
  Function31,
  Function32,
  Function33,
};
