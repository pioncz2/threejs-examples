const ParticleShader = {
  vertexShader: `
  attribute float size;
  varying vec2 vUv;
  varying vec3 vColor;
  varying float distToCamera;

  void main() {
    vUv = uv;
    vColor = color;
    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * mvPosition;
    gl_PointSize = size * ( 250.0 / -mvPosition.z ) + 2.5;
    distToCamera = -mvPosition.z;
  }
    `,

  fragmentShader: `
  uniform sampler2D tDiffuse;
  uniform sampler2D particleTexture;
  varying vec2 vUv;
  varying vec3 vColor;
  varying float distToCamera;

  void main(){
    vec4 colA = vec4( vColor, 1.0 );
    colA = colA * texture2D( particleTexture, gl_PointCoord );

    float distParsed = (250.0 / distToCamera);

    vec4 tint = vec4(0.21, 0.41, 0.71, 1.0);

    vec4 colB = colA * tint; // * distParsed;
    colB = vec4(
      colB.r * distParsed,
      colB.g * distParsed,
      colB.b * distParsed,
      colB.a
    );

    gl_FragColor = colB;
  }
    `,
};

export default ParticleShader;
