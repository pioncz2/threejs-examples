const ParticleTextureShader = {
  uniforms: {
    tDiffuse: { value: null },
  },

  vertexShader: `
  attribute float size;
  attribute vec2 pxPosition;
  varying vec2 vUv;
  varying vec3 vColor;
  varying vec4 mvPosition;
  varying vec2 pos;
  
  varying float distToCamera;

  void main() {
    vUv = uv;
    vColor = color;
    mvPosition = modelViewMatrix * vec4(position, 1.0);
    gl_Position = projectionMatrix * mvPosition;
    pos = pxPosition;
    gl_PointSize = size * ( 250.0 / -mvPosition.z ) + 0.5;
    distToCamera = -mvPosition.z;
  }
    `,

  fragmentShader: `
  uniform sampler2D tDiffuse;
  uniform sampler2D particleTexture;
  varying vec2 vUv;
  varying vec3 vColor;
  varying vec4 mvPosition;
  varying vec2 pos;
  varying float distToCamera;

  void main(){
    vec4 colA = vec4( vColor, 1.0 );
    // vec2 point = vec2( mvPosition.x, mvPosition.y );
    //vec2 point = vec2( pos.y, pos.x );
    vec2 point = vec2( pos.x, pos.y );
    colA = texture2D( particleTexture, point );

    float distParsed = (250.0 / distToCamera);

    vec4 tint = vec4(0.21, 0.41, 0.71, 1.0);

    vec4 colB = colA * tint; // * distParsed;
    // colB = vec4(
    //   colB.r * distParsed,
    //   colB.g * distParsed,
    //   colB.b * distParsed,
    //   colB.a
    // );

    gl_FragColor = colA;
  }
    `,
};

export default ParticleTextureShader;
