const noise2d = `
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//
vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}
float noise2d(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);
// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
    + i.x + vec3(0.0, i1.x, 1.0 ));
  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;
// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;
// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}`;

const LoopShader = {
  uniforms: {},

  vertexShader: `
    precision highp float;
    attribute vec3 position;
    attribute vec3 normal;
    attribute vec2 uv;
    uniform mat4 modelViewMatrix;
    uniform mat4 modelMatrix;
    uniform mat3 normalMatrix;
    uniform mat4 projectionMatrix;
    uniform vec3 cameraPosition;
    uniform float time;
    uniform float offset;
    varying vec3 vPosition;
    varying vec2 vUv;
    varying float vDepth;
    varying float vRim;
    varying vec2 vN;
    #define PI 3.1415926535897932384626433832795
    #define TAU (2.*PI)
    void main() {
      vUv = uv;
      vec3 p = position;
      vec4 mvPosition = modelViewMatrix * vec4( p, 1. );
      vPosition = (modelMatrix*vec4(position,1.)).xyz;
      gl_Position = projectionMatrix * mvPosition;
      vDepth = 1.5-20.*abs(gl_Position.z);
      vec3 e = normalize( (modelViewMatrix*vec4(cameraPosition,1.)).xyz);
      vec3 n = normalize( normalMatrix * normal );
      vRim = pow(abs(dot(e,n)),2.);
      e = normalize( mvPosition.xyz);
      vec3 r = reflect( e, n );
      float m = 2.82842712474619 * sqrt( r.z+1.0 );
      vN = r.xy / m + .5;
    }
    `,

  fragmentShader: `
    precision highp float;
    uniform float time;
    uniform float offset;
    uniform vec3 color;
    uniform float speed;
    uniform sampler2D matCap;
    varying vec2 vUv;
    varying float vDepth;
    varying vec3 vPosition;
    varying float vRim;
    varying vec2 vN;
    #define PI 3.1415926535897932384626433832795
    #define TAU (2.*PI)
    float parabola( float x, float k ){
        return pow( 4.0*x*(1.0-x), k );
      }
    ${noise2d}
    vec4 screen(vec4 base, vec4 blend, float opacity) {
        vec4 color = 1. - (1.-base) * (1.-blend);
        color = color * opacity + base * ( 1. - opacity );
        return color;
      }
    float v(vec2 uv, float offset, float t){
      float l = 10.;
      float o = .05 + .95*parabola(mod(1.*vUv.x+1.*t+offset+2.*time,1.),l);
      return o;
    }
    vec3 color1 = vec3(69., 91., 105.)/255.;
    vec3 color2 = vec3(249.,122.,77.)/255.;
    vec3 color3 = vec3(195.,58.,78.)/255.;
    void main(){
      float e = 1./3.;
      float t = -1.*time/5.;
      float o1 = v(vUv, 0., t);
      float o2 = v(vUv, e, t);
      float o3 = v(vUv, -e, t);
      float gradient = .5*(o1+o2+o3);
      vec2 s = vec2(50.,5.);
      vec2 uv = vUv + vec2(0.,time-10.*vUv.x);
      vec2 uv1 = floor(uv * s)/s;
      float r = parabola(mod(uv.x+4.*time/5.,1.), 3.);
      vec2 uv2 = mod(uv*s, vec2(1.));
      float lx = length(uv2.x-.5);
      float ly = length(uv2.y-.5);
      float l = max(lx,ly);
      if(step(r,l)>.5) discard;
      vec4 color = vRim*vec4(gradient*(o1*color1+o2*color2+o3*color3)/3.,1.);
      color += step(r-.05,l)/3.;
      float b = texture2D(matCap,vN).r/3.;
      gl_FragColor = screen(color, vec4(b), .75);
    }
    `,
};

export default LoopShader;
