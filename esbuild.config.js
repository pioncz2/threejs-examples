import esbuild from 'esbuild';
import serve, { error, log } from 'create-serve';

export const isWatch = process.argv.includes('-w');

const esbuildServe = async (options = {}) => {
  esbuild
    .build({
      ...options,
      watch: isWatch && {
        onRebuild(err) {
          const date = new Date();
          serve.update();
          err
            ? error('× Failed')
            : log(
                `✓ ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} Updated`,
              );
        },
      },
    })
    .catch((e) => {
      console.error(e);
      process.exit(1);
    });

  if (isWatch) {
    serve.start({
      // serve options (optional)
      port: 8080,
      root: 'public',
    });
  }
};

esbuildServe({
  // esbuild options
  entryPoints: ['src/index.js'],
  outfile: 'public/js/main.js',
  loader: {
    '.html': 'text',
  },
  bundle: true,
});
