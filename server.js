import express from 'express';
import http from 'http';

const port = 8080;

const app = express();
const httpInstance = http.Server(app);

app.use('/', express.static('public'));

httpInstance.listen(port, '0.0.0.0', function () {
  console.log('Listening on *:' + port);
});
